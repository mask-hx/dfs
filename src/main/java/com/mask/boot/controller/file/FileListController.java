package com.mask.boot.controller.file;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.FileList;
import com.mask.boot.util.ResultEntity;


/**
 * ClassName FileListController
 * Create by hx
 * Date 2021/5/30 3:05 下午
 */
@RestController
public class FileListController {
    @Autowired
    private FileList fileList;


    /**
     * 获取文件列表分页展示
     * @param pageIndex
     * @param pageSize
     * @return
     */
    @RequestMapping("/get/file/list")
    public ResultEntity<Page<FileList>> getFileList(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize
    ){
        try {
            int count = fileList.selectCount(new EntityWrapper().ne("file_id", 0));
            Page<FileList> list = fileList.selectPage(
                    new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileList>()
                    .orderBy("file_create_time",false)
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("抱歉！没有查询到您搜索的数据");
        }

    }

    @RequestMapping("/get/file/List/count")
    public ResultEntity<Integer> getFileListCount(){
        int count = fileList.selectCount(new EntityWrapper().ne("file_id",0));

        return ResultEntity.successWithData(count);
    }

    @RequestMapping("/admin/update/file")
    public String adminUpdateFile(
            @RequestParam("fileId") Integer fileId,
            @RequestParam("fileName") String fileName,
            @RequestParam("description") String description
    ){
        fileList.setFileId(fileId);
        fileList.setFileName(fileName);
        fileList.setDescription(description);
        boolean flag = fileList.updateById();
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }
}
