package com.mask.boot.util;



import com.mask.boot.constant.ConstantMap;

import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

/**
 * ClassName MD5Util
 * Create by hx
 * Date 2021/5/23 1:47 下午
 */

public class MD5Util {
    /**
     * 对明文字符串进行MD5加密
     * @author hx
     * @param source
     * @return 加密结果
     */
    public static String md5(String source){
        //1.判断source是否有效
        if (source == null || source.length() == 0){
            //2.如果不是有效的字符串抛出异常
            throw new RuntimeException(ConstantMap.MESSAGE_STRING_INVALIDATE);
        }
        //3.获取messageDigest对象
        String algorithm ="md5";
        try {
            MessageDigest messageDigest = MessageDigest.getInstance(algorithm);
            //4.获取明文字符串对应的字节数组
            byte[] input = source.getBytes();
            //5.执行加密
            byte[] output = messageDigest.digest(input);
            //6.创建BigInteger对象
            int signum =1;
            BigInteger bigInteger = new BigInteger(signum, output);
            //7.按照十六进制将bigInteger的值转换成字符串
            int radix =16;
            String encoded = bigInteger.toString(radix).toUpperCase();

            return encoded;
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        return null;
    }
}
