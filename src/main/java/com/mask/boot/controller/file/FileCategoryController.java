package com.mask.boot.controller.file;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mask.boot.entity.FileCategory;
import com.mask.boot.dao.FileCategoryMapper;
import com.mask.boot.dao.FileListMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.FileList;
import com.mask.boot.entity.PersonFile;
import com.mask.boot.util.ResultEntity;


import java.util.List;

/**
 * ClassName FileCategoryController
 * Create by hx
 * Date 2021/5/31 9:41 下午
 */

@RestController
public class FileCategoryController {
    @Autowired
    private FileList fileList;
    @Autowired
    private FileListMapper fileListMapper;
    @Autowired
    private FileCategory fileCategory;
    @Autowired
    private FileCategoryMapper fileCategoryMapper;
    @Autowired
    private PersonFile personFile;

    @RequestMapping("/get/file/category")
    public ResultEntity<List> getFileCategory(){
        try {
            List<FileCategory> list = fileCategory.selectAll();
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            return ResultEntity.failed("文件分类查询失败!");
        }
    }

    @RequestMapping("/admin/get/file/category")
    public ResultEntity<Page<FileCategory>> adminGetFileCategory(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize
    ){
        try {
            int count = fileCategory.selectCount(new EntityWrapper().ne("file_category_key","-1"));
            Page<FileCategory> list= fileCategory.selectPage(new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileCategory>());
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("抱歉！没有查询到您搜索的数据");
        }
    }

    @RequestMapping("/get/file/list/by/category")
    public ResultEntity<Page> getFileListByCategory(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize,
            @RequestParam(value = "categoryName") String categoryName,
            String fileAuthor
    ){
        if (!"私有文件".equals(categoryName)){
            try {
                //获取查询分类的总记录数
                int count = fileList.selectCount(new EntityWrapper().eq("file_category_key", categoryName));
                Page<FileList> list = fileList.selectPage(
                        new Page<>(pageIndex, pageSize),
                        new EntityWrapper<FileList>()
                                .eq("file_category_key", categoryName)
                                .orderBy("file_create_time",false)
                );
                list.setSize(count);
                return ResultEntity.successWithData(list);
            }catch (Exception e){
                e.printStackTrace();
                return ResultEntity.failed("查询分类文件列表失败!");
            }
        }else {
            try {
                int count = personFile.selectCount(new EntityWrapper()
                        .eq("file_author", fileAuthor)
                        .eq("file_category_key", categoryName)
                );
                Page<PersonFile> list = personFile.selectPage(
                        new Page<>(pageIndex, pageSize),
                        new EntityWrapper<PersonFile>()
                                .eq("file_category_key", categoryName)
                                .eq("file_author",fileAuthor)
                                .orderBy("file_create_time",false)
                );
                list.setSize(count);
                return ResultEntity.successWithData(list);
            }catch (Exception e){
                e.printStackTrace();
                return ResultEntity.failed("查询分类文件列表失败!");
            }
        }
    }

    @RequestMapping("/get/file/list/by/date")
    public ResultEntity<Page<FileList>> getFileListByDate(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize,
            @RequestParam(value = "date") String date
    ){
        try {
            //获取查询分类的总记录数
            int count = fileList.selectCount(new EntityWrapper().like("file_create_time",date));
            Page<FileList> list = fileList.selectPage(
                    new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileList>()
                            .like("file_create_time", date)
                            .orderBy("file_create_time",false)
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("查询分类文件列表失败!");
        }

    }

    @RequestMapping("/get/file/list/all/condition")
    public ResultEntity<Page<FileList>> getFileListAllCondition(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize,
            @RequestParam(value = "fileCategory",defaultValue = "") String fileCategory,
            @RequestParam(value = "fileAuthor",defaultValue = "") String fileAuthor,
            @RequestParam(value = "fileCreateTime",defaultValue = "") String fileCreateTime
    ){
        try {
            //获取查询分类的总记录数
            int count = fileList.selectCount(new EntityWrapper()
                    .eq("file_category_key",fileCategory)
                    .and().eq("file_author",fileAuthor)
                    .and().like("file_create_time",fileCreateTime));
            Page<FileList> list = fileList.selectPage(
                    new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileList>()
                            .eq("file_category_key",fileCategory)
                            .and().eq("file_author",fileAuthor)
                            .and().like("file_create_time",fileCreateTime)
                            .orderBy("file_create_time",false)
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("查询分类文件列表失败!");
        }

    }

    /**
     * 文件关键字查询
     * @param pageIndex
     * @param pageSize
     * @param keyword
     * @return
     */
    @RequestMapping("/get/file/list/by/keyword")
    public ResultEntity<Page<FileList>> getFileListByKeyword(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize,
            @RequestParam(value = "keyword",defaultValue = "") String keyword
    ){
        try {
            //获取查询分类的总记录数
            int count = fileList.selectCount(new EntityWrapper()
                    .like("file_name",keyword)
                    .or().like("file_author",keyword)
                    .or().like("file_create_time",keyword)
                    .or().like("file_category_key",keyword)
                    .or().like("file_download_times",keyword)
                    .or().like("description",keyword));
            Page<FileList> list = fileList.selectPage(
                    new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileList>()
                            .like("file_name",keyword)
                            .or().like("file_author",keyword)
                            .or().like("file_create_time",keyword)
                            .or().like("file_category_key",keyword)
                            .or().like("file_download_times",keyword)
                            .or().like("description",keyword)
                            .orderBy("file_create_time",false)
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("查询分类文件列表失败!");
        }

    }

    @RequestMapping("/admin/add/file/category")
    public String adminAddFileCategory(
            @RequestParam("fileCategoryKey") String fileCategoryKey,
            @RequestParam("fileCategoryName") String fileCategoryName
    ){

        boolean flag = fileCategoryMapper.adminInsertFileCategory(fileCategoryKey,fileCategoryName);
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/admin/delete/file/category")
    public String adminDeleteFileCategory(@RequestParam("fileCategoryKey") String fileCategoryKey){

        fileCategory.setFileCategoryKey(fileCategoryKey);
        boolean flag = fileCategory.deleteById();
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/admin/update/file/category")
    public String adminUpdateFileCategory(
            @RequestParam("fileCategoryKey") String fileCategoryKey,
            @RequestParam("fileCategoryName") String fileCategoryName,
            @RequestParam("fileCategoryOldName") String fileCategoryOldName
    ){
        fileCategory.setFileCategoryKey(fileCategoryKey);
        fileCategory.setFileCategoryName(fileCategoryName);
        boolean fileListFlag = fileListMapper.adminUpdateFileListForCategoryKey(fileCategoryName, fileCategoryOldName);
        boolean flag = fileCategory.updateById();
        if (flag && fileListFlag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }

    }



}
