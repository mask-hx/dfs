package com.mask.boot.controller.file;

import org.springframework.web.bind.annotation.*;
import org.springframework.web.multipart.MultipartFile;
import com.mask.boot.entity.FileList;
import com.mask.boot.entity.InfoMsg;
import com.mask.boot.entity.PersonFile;
import com.mask.boot.util.OSSUtil;
import com.mask.boot.util.ResultEntity;


import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.ByteArrayInputStream;
import java.io.IOException;

/**
 * ClassName ${NAME}
 * Create by hx
 * Date 2020/12/10 下午1:36
 */
@RestController
@RequestMapping("/upload")
public class FileUploadController{

    final long MAX_SIZE = 100 * 1024 * 1024;


//    @RequestMapping(value = "", method = RequestMethod.GET)
//    public String fileUploadInit() {
//        // InfoMsg infoMsg = new InfoMsg();
//
//        return ResultEntity.SUCCESS;
//    }

    @RequestMapping(value = "",method = RequestMethod.POST)
    public String uploadFile(
            @RequestParam("categoryKey") String categoryKey,
            @RequestParam("description") String description,
            @RequestParam("fileAuthor") String fileAuthor,
            @RequestParam("icon") MultipartFile file,
            HttpServletRequest request,
            HttpServletResponse response) throws IOException {

        String fileName = file.getOriginalFilename();
        InfoMsg infoMsg = new InfoMsg();
        if (file.isEmpty()) {
            infoMsg.setCode("error");
            infoMsg.setMsg("Please select a file to upload");
            return ResultEntity.FAILED;
        }
        try {
            ResultEntity<String> resultEntity = OSSUtil.uploadFileToOss(new ByteArrayInputStream(file.getBytes()), fileName);
            System.out.println(resultEntity);
            //文件上传成功后添加数据到数据库
            if ("私有文件".equals(categoryKey)){
                PersonFile personFile = new PersonFile(null, fileName, fileAuthor, null, categoryKey, null, description);
                personFile.insert();
            }else {
                FileList fileList = new FileList(null,fileName,fileAuthor,null,categoryKey,null,description);
                fileList.insert();
            }
            infoMsg.setCode("success");
            infoMsg.setMsg("You successfully uploaded '" + file.getOriginalFilename() + "'");
        }catch (Exception e){
            e.printStackTrace();
            infoMsg.setCode("error");
            infoMsg.setMsg("Uploaded file failed");
        }
        return ResultEntity.SUCCESS;

    }

//    /**
//     * 获取实时长传进度
//     *
//     * @param request
//     * @return int percent
//     */
//    @RequestMapping(value = "/to/upload/file",method = RequestMethod.GET)
//    public void getUploadPercent(HttpServletRequest request,HttpServletResponse response) throws IOException {
//        HttpSession session = request.getSession();
//        String percent = (String) session.getAttribute("upload_percent");
//        response.getWriter().print(percent);
//
//    }
//
//
//    /**
//     * 重置上传进度 前端调用进度之前调用此接口
//     *
//     * @param request
//     * @return void
//     */
//    @PostMapping("/resetPercent")
//    public String resetPercent(HttpServletRequest request) {
//        HttpSession session = request.getSession();
//        session.setAttribute("upload_percent", 0);
//        return ResultEntity.SUCCESS;
//    }

}
