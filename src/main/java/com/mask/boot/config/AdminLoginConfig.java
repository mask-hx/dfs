package com.mask.boot.config;

import com.mask.boot.interceptor.AdminLoginInterceptor;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

/**
 * //拦截器配置
 * ClassName AdminLoginConfig
 * Create by hx
 * Date 2021/6/10 2:41 下午
 */
@Configuration
public class AdminLoginConfig implements WebMvcConfigurer {
    @Override
    public void addInterceptors(InterceptorRegistry registry) {
        //注册AdminLoginInterceptor
        InterceptorRegistration registration = registry.addInterceptor(new AdminLoginInterceptor());
        registration.addPathPatterns("/admin/**") //拦截管理员页面
                .excludePathPatterns("/admin/login.html") //不拦截登录页面
                .excludePathPatterns("/admin/to/login") //不拦截登录请求
                .excludePathPatterns("/admin/logout") //不拦截退出登录请求
                ;

    }
}
