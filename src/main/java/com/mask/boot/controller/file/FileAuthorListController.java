package com.mask.boot.controller.file;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.FileList;
import com.mask.boot.util.ResultEntity;


/**
 * ClassName FileAuthorListController
 * Create by hx
 * Date 2021/6/1 5:16 下午
 */
@RestController
public class FileAuthorListController {
    @Autowired
    private FileList fileList;

    @RequestMapping("/get/file/author/list")
    public ResultEntity<Page<FileList>> getFileAuthorList(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize,
            @RequestParam(value = "fileAuthor") String fileAuthor
    ){
        try {
            //获取查询分类的总记录数
            int count = fileList.selectCount(new EntityWrapper().eq("file_author", fileAuthor));
            Page<FileList> list = fileList.selectPage(
                    new Page<>(pageIndex, pageSize),
                    new EntityWrapper<FileList>()
                            .eq("file_author", fileAuthor)
                            .orderBy("file_create_time",false)
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("查询用户文件列表失败!");
        }


    }
}
