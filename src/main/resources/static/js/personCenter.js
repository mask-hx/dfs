// window.pc_id = null;
// window.pc_username = null;
// window.pc_nick = null;
// window.pc_phone = null;
// window.pc_registerTime = null;

window.onload = function (){
    $.ajax({
        url:'/get/consumer/session',
        type:'get',
        datatype:'json',
        async:false,
        success:function (data){
            let result = data.result;
            if (result == "SUCCESS"){
                let list = data.data;
                window.id = list[0].id;
                window.username = list[0].username;
                window.nick = list[0].nick;
                window.phone = list[0].phone;
                window.registerTime = list[0].registerTime.substr(0,10)
                $("#login").text('');
                $("#alreadyLogin").text("欢迎您,"+nick);
                $("#logout").append("<img src='/images/logout.png' class='logout-img' alt='退出登录'>")
                $("#pc_username").val(username)
                $("#pc_nick").val(nick)
                $("#pc_phone").val(phone)
                $("#pc_time").val(registerTime)
            }

        }
    })
    updateConsumerDetailInfo();




}
function updateConsumerDetailInfo(){
    $.ajax({
        url:'/get/consumer/detail/info',
        type:'post',
        data:{
            username:username,
        },
        async:false,
        datatype:'json',
        success:function (data) {
            let result = data.result;
            if (result == "SUCCESS") {
                let list = data.data;
                pc_id = list[0].id;
                username = list[0].username;
                nick = list[0].nick;
                phone = list[0].phone;
                registerTime = list[0].registerTime.substr(0, 10);
                $("#login").text('');
                $("#alreadyLogin").text("欢迎您," + nick);
                // $("#logout").append("<img src='/images/logout.png' class='logout-img' alt='退出登录'>")
                $("#pc_username").val(username)
                $("#pc_nick").val(nick)
                $("#pc_phone").val(phone)
                $("#pc_time").val(registerTime)
            }
        }
    })
}

$(function (){
    let updateDetail = $(".updateDetail");
    //点击修改信息按钮
    updateDetail.click(function (){
        $("#consumerUpdateModal").modal("show");
        $("#register_username").val(username);
        $("#pc_update_nick").val(nick);
        $("#pc_update_phone").val(phone);
    })

    //昵称更新
    $("#pc_update_nick").keyup(function (){
        if ($("#pc_update_nick").val() == ''){
            $("#pc_update_nick").addClass("is-invalid");
            $("#message_pc_nick").text("昵称不能为空!");
        }else {
            $.ajax({
                url: '/update/nick',
                type: 'post',
                data: {
                    id:id,
                    nick:$("#pc_update_nick").val()
                },
                datatype: 'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#pc_update_nick").removeClass("is-invalid");
                        $("#pc_update_nick").addClass("is-valid");
                        $("#message_pc_nick").text('');
                    }
                    if (data == "FAILED"){
                        $("#pc_update_nick").removeClass("is-valid");
                        $("#pc_update_nick").addClass("is-invalid");
                        $("#message_pc_nick").text("昵称已被占用!");
                    }
                }
            })
        }
    })

    //手机号更新
    $("#pc_update_phone").keyup(function (){
        let regex = /^1[34578]\d{9}$/;
        if ($("#pc_update_phone").val() == ''){
            $("#pc_update_phone").addClass("is-invalid")
            $("#pc_update_phone").text("请填写手机号!");
        }else if (!regex.test($("#pc_update_phone").val())){
            $("#pc_update_phone").addClass("is-invalid");
            $("#message_pc_phone").text("手机号格式有误!")
        }else {
            $.ajax({
                url:'/update/phone',
                type:'post',
                data:{
                    id:id,
                    phone:$("#pc_update_phone").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#message_pc_phone").text('');
                        $("#pc_update_phone").removeClass("is-invalid");
                        $("#pc_update_phone").addClass("is-valid");
                    }
                    if (data == "FAILED"){
                        $("#message_pc_phone").text("该手机号已被注册!");
                        $("#pc_update_phone").removeClass("is-valid");
                        $("#pc_update_phone").addClass("is-invalid");
                    }
                }
            })
        }
    })

    //用户更新信息验证码校验
    $("#short_message_input").keyup(function (){
        if ($("#short_message_input").val() == ''){
            $("#message_register_short").text("验证码不能为空!");
        }else {
            $.ajax({
                url:'/get/short/message',
                type:'get',
                data:{
                    phone:$("#pc_update_phone").val(),
                    message:$("#short_message_input").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "codeNotMatch"){
                        $("#message_register_short").text("验证码不正确!")
                        $("#message_register_short_success").text("");
                    }else if (data == "codeTimeout"){
                        $("#message_register_short").text("验证码已过期!");
                        $("#message_register_short_success").text("");
                    }else if (data == "SUCCESS"){
                        $("#message_register_short").text("");
                        $("#message_register_short_success").text("验证码正确!");
                    }else {
                        $("#message_register_short").text("请求超时!");
                        $("#message_register_short_success").text("");
                    }
                }
            })
        }
    })
})
//修改个人信息
//短信点击后倒计时
let personTime = 60;
function getPersonCountDown(){
    if (personTime === 1){
        personTime = 60;
        $(".phone-in-input").text("发送验证码")
        $(".phone-in-input").removeAttr("disabled")
        return;
    }else {
        personTime --;
        $(".phone-in-input").text(personTime+"s");
        $(".phone-in-input").attr("disabled",true);
    }
    setTimeout(function (){
        getPersonCountDown();
    },1000);
}

//获取短信
function getPersonPhoneMessage(){
    let regex = /^1[34578]\d{9}$/; //手机号正则匹配
    if ($("#pc_update_phone").val() == ''){
        $("#pc_update_phone").addClass("is-invalid")
        $("#message_pc_phone").text("请填写手机号!");
    }else if (!regex.test($("#pc_update_phone").val())){
        $("#pc_update_phone").addClass("is-invalid");
        $("#message_pc_phone").text("手机号格式不正确")
    }else {
        $.ajax({
            url:'/send/phone',
            type:'get',
            data:{
                phone:$("#pc_update_phone").val()
            },
            datatype:'json',
            success:function (data){
                let result = data.result;
                if (result == "SUCCESS"){
                    layer.msg("发送成功!");
                }
                getPersonCountDown();
                if (result == "FAILED"){
                    layer.msg("请求失败!");
                }

            }
        })
    }

}




function updatePersonDetail(){
    let regex = /^1[34578]\d{9}$/;

    if ($("#register_password").val() == ''){
        layer.msg("密码不能为空!");
        return;
    }

    if ($("#register_confirm_password").val() == ''){
        layer.msg("确认密码不能为空!");
        return;
    }

    if ($("#register_password").val() != $("#register_confirm_password").val()){
        layer.msg("两次密码不一致!");
        return;
    }

    if ($("#pc_update_nick").val() == ''){
        layer.msg("昵称不能为空!");
        return;
    }

    if ($("#pc_update_phone").val() == ''){
        layer.msg("手机号不能为空!");
        return;
    }

    if (!regex.test($("#pc_update_phone").val())){
        layer.msg("手机号格式有误!");
        return;
    }

    if ($("#short_message_input").val() == ''){
        layer.msg("验证码不能为空!");
        return;
    }

    let flag1 = 1;
    let flag2 = 1;
    let flag3 = 1;

    $.ajax({
        url: '/get/username',
        type: 'post',
        data: {
            register_username:$("#register_username").val()
        },
        datatype: 'json',
        success:function (data){
            // if (data == "FAILED"){
            //     layer.msg("账户名已被占用!");
            //     flag1 = 0;
            // }
            if (flag1 == 1){
                $.ajax({
                    url: '/update/nick',
                    type: 'post',
                    data: {
                        id:id,
                        nick:$("#pc_update_nick").val()
                    },
                    datatype: 'json',
                    success:function (data){
                        console.log(data);
                        if (data == "FAILED"){
                            layer.msg("昵称已被占用!");
                            flag2 = 0;
                        }
                        if (flag2 == 1){
                            $.ajax({
                                url:'/update/phone',
                                type:'post',
                                data:{
                                    id:id,
                                    phone:$("#pc_update_phone").val()
                                },
                                datatype:'json',
                                success:function (data){
                                    if (data == "FAILED"){
                                        layer.msg("该手机号已被注册!");
                                        flag3 = 0;
                                    }
                                    if (flag3 == 1){
                                        $.ajax({
                                            url:'/get/short/message',
                                            type:'get',
                                            data:{
                                                phone:$("#pc_update_phone").val(),
                                                message:$("#short_message_input").val()
                                            },
                                            datatype:'json',
                                            success:function (data){
                                                if (data == "codeNotMatch"){
                                                    layer.msg("验证码不正确!");
                                                    return;
                                                }else if (data == "codeTimeout"){
                                                    layer.msg("验证码已过期!")
                                                    return;
                                                }else if (data == "SUCCESS"){
                                                    $.ajax({
                                                        url:'/to/update/consumer',
                                                        type:'post',
                                                        data:{
                                                            id:id,
                                                            password:$("#register_password").val(),
                                                            oldNick:nick,
                                                            nick:$("#pc_update_nick").val(),
                                                            phone:$("#pc_update_phone").val(),
                                                        },
                                                        success:function (data){
                                                            if (data == "SUCCESS"){
                                                                $("#consumerUpdateModal").init();
                                                                $("#consumerUpdateModal").hide();
                                                                $(".modal-backdrop").remove();
                                                                //注册后的提示
                                                                window.location.reload();
                                                                layer.msg("更新成功!");
                                                                $(".newLoginNick").text("欢迎您,"+nick)

                                                            }
                                                            if (data == "FAILED"){
                                                                layer.msg("更新失败!");
                                                            }
                                                        }
                                                    })
                                                }else {
                                                    $("#message_register_short").text("请求超时!");
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}


