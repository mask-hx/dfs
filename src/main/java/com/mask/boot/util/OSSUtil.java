package com.mask.boot.util;

import com.aliyun.oss.OSS;
import com.aliyun.oss.OSSClientBuilder;
import com.aliyun.oss.common.comm.ResponseMessage;
import com.aliyun.oss.model.PutObjectResult;
import com.aliyun.oss.model.VoidResult;

import java.io.InputStream;

/**
 * ClassName OSSUtil
 * Create by hx
 * Date 2021/5/24 8:20 下午
 */

public class OSSUtil {

    /**
     * 专门负责上传文件到OSS服务器的工具方法
     * @param inputStream		要上传的文件的输入流
     * @param originalName		要上传的文件的原始文件名
     * @return	包含上传结果以及上传的文件在OSS上的访问路径
     */

    private static String endpoint = "oss-cn-shanghai.aliyuncs.com";
    private static String accessKeyId = "LTAI5t6etrkVaXGKBWqhVXLk";
    private static String accessKeySecret = "OlKeT2DSbvpxYpwzZ5R45610lwM5fL";
    private static String bucketDomain = "http://mask4-boot.oss-cn-shanghai.aliyuncs.com";
    private static String bucketName = "mask4-boot";

    /**
     * 获取oss实例
     * @return
     */
    public static OSS getOSSClient(){
        return new OSSClientBuilder().build(endpoint,accessKeyId,accessKeySecret);
    }

    public static ResultEntity<String> uploadFileToOss(
            InputStream inputStream,
            String originalName) {
        // 创建OSSClient实例
        OSS ossClient = getOSSClient();
        //OSS ossClient = new OSSClientBuilder().build(OSSProperties.endPoint, OSSProperties.accessKeyId, OSSProperties.accessKeySecret);

        // 生成上传文件的目录
        //String folderName = new SimpleDateFormat("yyyyMMdd").format(new Date());

        // 生成上传文件在OSS服务器上保存时的文件名
        // 原始文件名：beautfulgirl.jpg
        // 生成文件名：wer234234efwer235346457dfswet346235.jpg
        // 使用UUID生成文件主体名称
        //String fileMainName = UUID.randomUUID().toString().replace("-", "");

        // 从原始文件名中获取文件扩展名
        //String extensionName = originalName.substring(originalName.lastIndexOf("."));

        // 使用目录、文件主体名称、文件扩展名称拼接得到对象名称
//        String objectName = folderName + "/" + fileMainName + extensionName;
        String objectName = "mask4-file-upload/"+originalName;

        try {
            // 调用OSS客户端对象的方法上传文件并获取响应结果数据
            PutObjectResult putObjectResult = ossClient.putObject(bucketName, objectName, inputStream);

            // 从响应结果中获取具体响应消息
            ResponseMessage responseMessage = putObjectResult.getResponse();

            // 根据响应状态码判断请求是否成功
            if(responseMessage == null) {

                // 拼接访问刚刚上传的文件的路径
                String ossFileAccessPath = bucketDomain + "/" + objectName;

                // 当前方法返回成功
                return ResultEntity.successWithData(ossFileAccessPath);
            } else {
                // 获取响应状态码
                int statusCode = responseMessage.getStatusCode();

                // 如果请求没有成功，获取错误消息
                String errorMessage = responseMessage.getErrorResponseAsString();

                // 当前方法返回失败
                return ResultEntity.failed("当前响应状态码="+statusCode+" 错误消息="+errorMessage);
            }
        } catch (Exception e) {
            e.printStackTrace();

            // 当前方法返回失败
            return ResultEntity.failed(e.getMessage());
        } finally {

            if(ossClient != null) {

                // 关闭OSSClient。
                ossClient.shutdown();
            }
        }

    }

    /**
     * 删除单个文件
     * @param filePath
     * @return
     */
    public static Boolean deleteOssFile(String filePath){

        //创建oss实例
        OSS ossClient = getOSSClient();

        //删除Object
        //boolean exist = ossClient.doesObjectExist(bucketName, filePath);
        //文件存在执行删除操作
        VoidResult voidResult = ossClient.deleteObject(bucketName, filePath);
        System.out.println(voidResult);
        ossClient.shutdown();
        return true;
//        if (exist){
//            //删除操作
//
//            //关闭ossClient
//            ossClient.shutdown();
//            return true;
//
//        }else {
//            return false;
//        }
    }

}
