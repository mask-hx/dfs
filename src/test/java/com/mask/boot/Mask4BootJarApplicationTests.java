package com.mask.boot;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import com.mask.boot.constant.ConstantMap;
import com.mask.boot.entity.FileCategory;
import com.mask.boot.dao.FileCategoryMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import com.mask.boot.entity.Consumer;
import com.mask.boot.util.OSSUtil;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.List;

@SpringBootTest
class Mask4BootJarApplicationTests {
    @Autowired
    private Consumer consumer;
    @Autowired
    FileCategory fileCategory;
    @Autowired
    private FileCategoryMapper fileCategoryMapper;

    @Test
    void contextLoads() {
    }
    @Test
    public void testDeleteFile() throws IOException {
        Path path = Paths.get("/Users/hx/FILE/test/sun.JPG");
        Files.delete(path);
    }

    @Test
    public void testDeleteOSSFile(){
        Boolean aBoolean = OSSUtil.deleteOssFile(ConstantMap.OSS_FILE_UPLOAD_PATH + "马可波罗.jpg");
        System.out.println(aBoolean);
    }

    @Test
    public void selectConsumerList(){
        int count = consumer.selectCount(new EntityWrapper().ne("id", 0));
        Page<Consumer> list = consumer.selectPage(new Page<>(1, 8),
                new EntityWrapper<>()
        );
        list.setSize(count);
        System.out.println(list);
        System.out.println(list.getRecords());

    }

    @Test
    public void testLogin(){
        List<Consumer> list = consumer.selectList(new EntityWrapper()
                .eq("username", "mask")
        );
        System.out.println(list);
        Consumer consumer = list.get(0);
        Integer isLive = consumer.getIsLive();
        System.out.println(isLive);

    }

    @Test
    public void testSelectFileCategoryList(){
        int count = fileCategory.selectCount(new EntityWrapper().ne("file_category_key","-1"));
        Page<FileCategory> list= fileCategory.selectPage(new Page<>(1, 8),
                new EntityWrapper<FileCategory>());
        list.setSize(count);
        System.out.println(list);
        System.out.println(list.getRecords());
    }
    @Test
    public void testGit(){
    }
//    @Test
//    public void testGitMaven(){
//
//    }

}
