package com.mask.boot.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import com.mask.boot.entity.FileCategory;


/**
 * ClassName UserMapper
 * Create by hx
 * Date 2021/5/26 8:37 上午
 */
@Mapper
public interface FileCategoryMapper extends BaseMapper<FileCategory> {

    @Insert("insert into file_category values(#{fileCategoryKey},#{fileCategoryName})")
    Boolean adminInsertFileCategory(@Param("fileCategoryKey") String fileCategoryKey,@Param("fileCategoryName") String fileCategoryName);
}
