package com.mask.boot.controller.file;

import com.mask.boot.constant.ConstantMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.FileList;
import com.mask.boot.entity.PersonFile;
import com.mask.boot.util.OSSUtil;
import com.mask.boot.util.ResultEntity;

/**
 * ClassName FileDeleteController
 * Create by hx
 * Date 2021/6/3 2:57 下午
 */

@RestController
public class FileDeleteController {
    @Autowired
    private FileList fileList;
    @Autowired
    private PersonFile personFile;

    @RequestMapping("/to/delete/file")
    public String deleteOneFile(
            @RequestParam("fileId") Integer fileId,
            @RequestParam("fileCategoryKey") String fileCategoryKey,
            @RequestParam("fileName") String fileName
    ){
        try {
            if ("私有文件".equals(fileCategoryKey)){
                boolean deletePersonFileOne = personFile.deleteById(fileId);
                //OSS对象存储删除对应文件
                String filePath = ConstantMap.OSS_FILE_UPLOAD_PATH+fileName;
                Boolean result = OSSUtil.deleteOssFile(filePath);
                if (deletePersonFileOne && result){
                    return ResultEntity.SUCCESS;
                }else {
                    return ResultEntity.FAILED;
                }

            }else {
                boolean deleteFileListOne = fileList.deleteById(fileId);
                String filePath = ConstantMap.OSS_FILE_UPLOAD_PATH+fileName;
                Boolean result = OSSUtil.deleteOssFile(filePath);
                if (deleteFileListOne && result){
                    return ResultEntity.SUCCESS;
                }else {
                    return ResultEntity.FAILED;
                }
            }
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.FAILED;
        }
    }
}
