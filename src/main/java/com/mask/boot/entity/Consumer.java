package com.mask.boot.entity;


import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * ClassName User
 * Create by hx
 * Date 2020/11/22 下午8:32
 */
@TableName("m_consumer")
@Data
@AllArgsConstructor
@NoArgsConstructor
@Component
public class Consumer extends Model<Consumer> {
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private String username;
    private String password;
    private String nick;
    private String phone;
    private Integer isLive;
    private Date registerTime;

    @Override
    protected Serializable pkVal() {
        return id;
    }
}
