package com.mask.boot.interceptor;

import org.springframework.web.servlet.HandlerInterceptor;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**管理员登录页面拦截
 * ClassName AdminLoginInterceptor
 * Create by hx
 * Date 2021/6/10 2:33 下午
 */

public class AdminLoginInterceptor implements HandlerInterceptor {

    @Override
    public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler) throws Exception {
//        System.out.println("执行前");
        try {
            Object admin = request.getSession().getAttribute("admin");
//            System.out.println(admin);
            if (admin != null){
                return true;
            }else {
                //没有登录权限跳转到首页
                response.sendRedirect("/");
            }
        }catch (Exception e){
            e.printStackTrace();
        }
        return false;  //设置为false拦截器到此处不会再执行,设置为true会继续执行
    }

    @Override
    public void afterCompletion(HttpServletRequest request, HttpServletResponse response, Object handler, Exception ex) throws Exception {

    }
}
