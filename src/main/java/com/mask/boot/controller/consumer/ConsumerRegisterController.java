package com.mask.boot.controller.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.mask.boot.constant.ConstantMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;
import com.mask.boot.util.MD5Util;
import com.mask.boot.util.RedisUtil;
import com.mask.boot.util.ResultEntity;
import com.mask.boot.util.ShortMessageUtil;


import java.util.List;

/**
 * ClassName ConsumerController
 * Create by hx
 * Date 2021/5/26 11:08 下午
 */
@RestController
public class ConsumerRegisterController {
    @Autowired
    private RedisUtil redisUtil;

    @Autowired
    private Consumer consumer;
    /**
     * 判断注册用户名
     * @param register_username
     * @return
     */
    @RequestMapping("/get/username")
    public String judgeUsername(@RequestParam("register_username") String register_username){
        List<Consumer> list = consumer.selectList(
                new EntityWrapper<Consumer>()
                        .eq("username",register_username));
        if (list.size() == 0){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    /**
     * 判断注册昵称
     * @param nick
     * @return
     */
    @RequestMapping("/get/nick")
    public String judgeNick(@RequestParam("nick") String nick){
        List<Consumer> list = consumer.selectList(
                new EntityWrapper()
                        .eq("nick", nick));
        if (list.size() == 0){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    /**
     * 注册用户发送手机验证码是否成功
     * @param phone
     * @return
     */
    @RequestMapping("/judge/phone")
    public String judgePhone(@RequestParam("phone") String phone){
        List<Consumer> list = consumer.selectList(new EntityWrapper().eq("phone", phone));
        if (list.size() == 0){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    /**
     * 判断验证码是否正确
     * @param phone
     * @return
     */
    @RequestMapping("/send/phone")
    public ResultEntity<String> sendPhone(@RequestParam("phone") String phone){
        //1.发送验证码到手机
        ResultEntity<String> sendMessageResultEntity = ShortMessageUtil.sendCodeByShortMessage(phone);
        //System.out.println(sendMessageResultEntity);
        //2.判断短信发送结果
        if (ResultEntity.SUCCESS.equals(sendMessageResultEntity.getResult())){
            //3.发送成功将验证码存入redis
            String code = sendMessageResultEntity.getData();
            String key = ConstantMap.REDIS_CODE_PREFIX + phone;
            //保存redis的key时间为1分钟
            ResultEntity<String> saveCodeResultEntity = redisUtil.set(key, code, 60);
            if (ResultEntity.SUCCESS.equals(saveCodeResultEntity.getResult())){
                return ResultEntity.successWithoutData();
            }else {
                return saveCodeResultEntity;
            }
        }
        System.out.println(sendMessageResultEntity);
        return sendMessageResultEntity;
    }

    /**
     * 调用短信平台发送手机验证码
     * @param phone
     * @param message
     * @return
     */
    @RequestMapping("/get/short/message")
    public String getMessage(@RequestParam("phone") String phone,@RequestParam("message") String message){
        //1.拼接redis中手机号的key
        String key = ConstantMap.REDIS_CODE_PREFIX + phone;
        //2.获取key中的value值
        ResultEntity<String> resultEntity = redisUtil.get(key);

        //获取resultEntity中获取结果信息
        String result = resultEntity.getResult();
        if (ResultEntity.FAILED.equals(result)){
            return ConstantMap.REDIS_CODE_ERROR;
        }
        //获取redis中的验证码
        String redisCode = resultEntity.getData();
        if (redisCode == null){
            return ConstantMap.REDIS_CODE_TIMEOUT;
        }
        if (!redisCode.equals(message)){
            return ConstantMap.REDIS_CODE_NOT_MATCH;
        }
        return resultEntity.getResult();
    }

    /**
     * 用户注册执行
     * @param consumer
     * @return
     */
    @RequestMapping("/to/register/consumer")
    public String register(Consumer consumer){
        //对密码进行md5加密
        String formPassword = consumer.getPassword();
        String md5Password = MD5Util.md5(formPassword);
        consumer.setPassword(md5Password);
        //AR数据库操作
        boolean flag = consumer.insert();
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }
}

