package com.mask.boot.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;

/**
 * ClassName FileCategory
 * Create by hx
 * Date 2021/5/29 8:24 下午
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("file_category")
@Component
public class FileCategory extends Model<FileCategory> {
    @TableId(value = "file_category_key")
    private String fileCategoryKey;
    private String fileCategoryName;

    @Override
    protected Serializable pkVal() {
        return fileCategoryKey;
    }
}
