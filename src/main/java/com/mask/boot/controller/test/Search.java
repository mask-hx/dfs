package com.mask.boot.controller.test;

/**
 * ClassName Search
 * Create by hx
 * Date 2021/6/16 7:13 下午
 */

public class Search {
    public static void main(String[] args) {
        int arr[] = {1,9,11,-1,34,89};
        int index = searchString(arr, 11);
        if (index == -1){
            System.out.println("没有找到");
        }else{
            System.out.println("下标为："+index);
        }
    }

    public static int searchString(int[] array ,int value){
        for (int i = 0; i < array.length; i++) {
            if (array[i] == value){
                return i;
            }
        }
        return -1;
    }
}
