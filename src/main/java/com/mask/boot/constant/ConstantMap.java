package com.mask.boot.constant;

/**
 * 常量类,存储提示信息
 * ClassName ConstantMap
 * Create by hx
 * Date 2021/5/26 11:27 下午
 */

public class ConstantMap {

    /**
     * 访问异常
     */
    public static final String ATTR_NAME_EXCEPTION = "exception";
    /**
     * 手机号存储到redis的key值前缀
     */
    public static final String REDIS_CODE_PREFIX = "REDIS_CODE_PREFIX_";


    public static final String MESSAGE_STRING_INVALIDATE = "字符串不合法，请不要传入空字符串!";

    /**
     * redis验证码不匹配
     */
    public static final String REDIS_CODE_NOT_MATCH = "codeNotMatch";

    /**
     * redis验证码已过期
     */
    public static final String REDIS_CODE_TIMEOUT = "codeTimeout";

    /**
     * redis访问失败
     */
    public static final String REDIS_CODE_ERROR = "error";

    /**
     * oss对象存储服务下载路径
     */
    public static final String OSS_FILE_UPLOAD_PATH = "https://mask4-boot.oss-cn-shanghai.aliyuncs.com/mask4-file-upload/";
}
