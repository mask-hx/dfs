package com.mask.boot.controller.admin;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;
import com.mask.boot.util.ResultEntity;

import javax.servlet.http.HttpServletRequest;

/**
 * ClassName AdminLoginController
 * Create by hx
 * Date 2021/6/9 9:19 下午
 */
@RestController
public class AdminController {
    @Autowired
    private Consumer consumer;

    @RequestMapping("/admin/to/login")
    public String adminToLogin(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            HttpServletRequest request
    ){
        //String md5Password = MD5Util.md5(password);
        //List<Consumer> list = consumer.selectList(new EntityWrapper().
                //eq("username", username).and().eq("password", md5Password));
//        if (list.size()==0){
//            return ResultEntity.FAILED;
//        }else {
//            request.getSession().setAttribute("admin",list);
//            return ResultEntity.SUCCESS;
//        }
        if ("admin".equals(username) && "admin".equals(password)){
            request.getSession().setAttribute("admin",username);
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/admin/logout")
    public String adminLogout(HttpServletRequest request){
        try {
            request.getSession().setAttribute("admin",null);
            return ResultEntity.SUCCESS;
        }catch (Exception e){
            return ResultEntity.FAILED;
        }
    }
}
