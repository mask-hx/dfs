package com.mask.boot.controller.file;

import com.mask.boot.dao.FileListMapper;
import com.mask.boot.dao.PersonFileMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.util.ResultEntity;

import javax.servlet.annotation.WebServlet;

/**
 * ClassName ${NAME}
 * Create by hx
 * Date 2020/11/21 下午5:42
 */
//@Controller
@WebServlet("/download")
@RestController
public class FileDownloadController{
    @Autowired
    private FileListMapper fileListMapper;
    @Autowired
    PersonFileMapper personFileMapper;

    @RequestMapping("/download/file/success/echo")
    public String downloadFile(
            @RequestParam("fileName") String fileName,
            @RequestParam("fileAuthor") String fileAuthor
            ) {

        //下载文件成功后添加文件下载次数
        try {
            Boolean personFileFlag = personFileMapper.addPersonFileDownloadTimes(fileName, fileAuthor);
        }catch (Exception e){
            e.printStackTrace();
        }
        //判断文件下载成功后添加下载次数是否成功
        Boolean fileListFlag = fileListMapper.addFileListDownloadTimes(fileName, fileAuthor);
        if (fileListFlag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

}
