package com.mask.boot.controller.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;
import com.mask.boot.util.MD5Util;
import com.mask.boot.util.ResultEntity;


import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * ClassName ConsumerLoginController
 * Create by hx
 * Date 2021/5/28 11:10 下午
 */
@RestController
public class ConsumerLoginController {

    /**
     * 用户登录判断用户名是否可用
     * @param username
     * @return
     */
    @RequestMapping("/to/login/judge/username")
    public String judgeLoginUsername(@RequestParam("username") String username){
        Consumer consumer = new Consumer();
        List<Consumer> list = consumer.selectList(new EntityWrapper().eq("username", username));
        if (list.size() == 0){
            return ResultEntity.FAILED;
        }else {
            return ResultEntity.SUCCESS;
        }
    }

    /**
     * 执行登录操作
     * @param username
     * @param password
     * @param rememberMe
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/to/login")
    public String login(
            @RequestParam("username") String username,
            @RequestParam("password") String password,
            Boolean rememberMe,
            HttpServletRequest request,
            HttpServletResponse response
    ){
        Consumer consumer = new Consumer();
        String md5Password = MD5Util.md5(password);
        List<Consumer> list = consumer.selectList(new EntityWrapper()
                .eq("username", username)
                .and().eq("password", md5Password)
        );
        Consumer consumer1 = list.get(0);
        Integer isLive = consumer1.getIsLive();
        //如果用户处于禁用状态则响应错误信息
        if (isLive==0){
            response.setStatus(700);
        }

        if (list.size() == 0){
            return ResultEntity.FAILED;
        }else {
            if (rememberMe){
                //勾选记住则添加cookie信息
                Cookie cookieUsername = new Cookie("username",username);
                Cookie cookiePassword = new Cookie("password",password);
                cookieUsername.setHttpOnly(false); //设置cookie能被js访问
                cookiePassword.setHttpOnly(false); //设置cookie能被js访问
                cookieUsername.setMaxAge(60 * 60 * 24 * 30); //保存30天
                cookiePassword.setMaxAge(60 * 60 * 24 * 30); //保存30天
                cookieUsername.setPath(request.getContextPath()+"/");//设置当前项目下都携带这个cookie
                cookiePassword.setPath(request.getContextPath()+"/");//设置当前项目下都携带这个cookie
                response.addCookie(cookieUsername);
                response.addCookie(cookiePassword);
                request.getSession().setAttribute("consumer",list);
            }else {
                //
                request.getSession().setAttribute("consumer",list);
                //不勾选记住则清除cookie
                Cookie cookieUsername = new Cookie("username", null);
                Cookie cookiePassword = new Cookie("password", null);
                cookieUsername.setMaxAge(0);
                cookiePassword.setMaxAge(0);
                cookieUsername.setPath(request.getContextPath()+"/");
                cookiePassword.setPath(request.getContextPath()+"/");
                response.addCookie(cookieUsername);
                response.addCookie(cookiePassword);
                return ResultEntity.SUCCESS;
            }
            return ResultEntity.SUCCESS;

        }
    }

    /**
     * 获取用户记住密码cookie
     * @param request
     * @return
     */
    @RequestMapping("/get/consumer/cookie")
    public ResultEntity getCookie(HttpServletRequest request){
        Cookie[] cookies = request.getCookies();
        if (cookies == null){
            return ResultEntity.failed("没有获取到cookie");
        }else {
            Map<String,String> map = new HashMap<>();
            for (Cookie cookie : cookies){
                String username = cookie.getName();
                if ("username".equals(username) || "password".equals(username)){
                    map.put(cookie.getName(),cookie.getValue());
                }
            }
            return ResultEntity.successWithData(map);

        }
    }

    /**
     * 获取用户登录后session信息
     * @param session
     * @return
     */
    @RequestMapping("/get/consumer/session")
    public ResultEntity<List> getSession(HttpSession session){
        try {
            List list = (List) session.getAttribute("consumer");
            if (list.size() == 0){
                return ResultEntity.failed("获取session失败");
            }else {
                return ResultEntity.successWithData(list);
            }
        }catch (Exception e){
            //e.printStackTrace();
        }
        return ResultEntity.failed("获取session失败");
    }

    /**
     * 退出登录
     * @param request
     * @param response
     * @return
     */
    @RequestMapping("/to/logout")
    public String logout(HttpServletRequest request,HttpServletResponse response){
        try {
            request.getSession().invalidate();
            return ResultEntity.SUCCESS;
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.FAILED;
        }

    }
}

