package com.mask.boot.controller.admin;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import com.baomidou.mybatisplus.plugins.Page;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;
import com.mask.boot.util.ResultEntity;

/**
 * ClassName ConsumerController
 * Create by hx
 * Date 2021/6/10 8:39 下午
 */
@RestController
public class ConsumerController {
    @Autowired
    private Consumer consumer;

    @RequestMapping("/admin/get/consumer/list")
    public ResultEntity<Page<Consumer>> getConsumerList(
            @RequestParam(value = "pageIndex",defaultValue = "1") Integer pageIndex,
            @RequestParam(value = "pageSize",defaultValue = "8") Integer pageSize
    ){
        try {
            int count = consumer.selectCount(new EntityWrapper().ne("id", 0));
            Page<Consumer> list = consumer.selectPage(new Page<>(pageIndex, pageSize),
                    new EntityWrapper<>()
            );
            list.setSize(count);
            return ResultEntity.successWithData(list);
        }catch (Exception e){
            e.printStackTrace();
            return ResultEntity.failed("抱歉！没有查询到您搜索的数据");
        }
    }
}
