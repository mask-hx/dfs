package com.mask.boot.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import com.mask.boot.entity.PersonFile;


/**
 * ClassName UserMapper
 * Create by hx
 * Date 2021/5/26 8:37 上午
 */
@Mapper
public interface PersonFileMapper extends BaseMapper<PersonFile> {
    @Update("UPDATE person_file set file_download_times=file_download_times+1 " +
            "WHERE file_name=#{fileName} AND file_author=#{fileAuthor}")
    Boolean addPersonFileDownloadTimes(@Param("fileName") String fileName, @Param("fileAuthor") String fileAuthor);
}
