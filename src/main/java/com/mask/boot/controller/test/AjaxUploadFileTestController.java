package com.mask.boot.controller.test;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import java.io.FileOutputStream;

/**
 * ClassName AjaxUploadFileTestController
 * Create by hx
 * Date 2021/6/7 5:26 下午
 */
@RestController
public class AjaxUploadFileTestController {
    @RequestMapping("/ajaxUploadTest")
    public String testUpload(
            @RequestParam("file") MultipartFile file,
            @RequestParam("valOne") String val1,
            @RequestParam("valTwo") String val2
            ){
        //获取文件名
        String fileName = file.getOriginalFilename();
        String path = "/Users/hx/FILE/test/";
        FileOutputStream out = null;
        try {
            out = new FileOutputStream(path+fileName);
            out.write(file.getBytes());
            out.flush();
            out.close();
        }catch (Exception e){
            e.printStackTrace();
            return "failed";
        }

        System.out.println(fileName);
        System.out.println(val1);
        System.out.println(val2);
        return "success";
    }

}
