package com.mask.boot.controller.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;

import com.mask.boot.util.ResultEntity;

import java.util.List;

/**
 * ClassName findConsumerListController
 * Create by hx
 * Date 2021/6/1 4:41 下午
 */
@RestController
public class findConsumerListController {
    @Autowired
    private Consumer consumer;
    @RequestMapping("get/consumer/list")
    public ResultEntity<List> getConsumerList(){
        try {
            List<Consumer> list = consumer.selectList(new EntityWrapper()
                    .setSqlSelect("id", "nick").orderBy("id", true));
            return ResultEntity.successWithData(list);

        }catch (Exception e){
            return ResultEntity.failed("查询用户列表失败!");
        }
    }
}
