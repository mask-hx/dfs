package com.mask.boot.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

/**访问首页
 * ClassName IndexController
 * Create by hx
 * Date 2021/5/26 8:11 上午
 */
@Controller
public class IndexController {

    @RequestMapping("/")
    public String index(){
        return "index.html";
    }
}
