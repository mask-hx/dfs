package com.mask.boot.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import com.mask.boot.entity.Consumer;


/**
 * ClassName UserMapper
 * Create by hx
 * Date 2021/5/26 8:37 上午
 */
@Mapper
public interface ConsumerMapper extends BaseMapper<Consumer> {

    @Update("update m_consumer set password=#{password} where username=#{username}")
    Boolean updatePasswordByUsername(@Param("password") String password,@Param("username") String username);


    @Update("update m_consumer set nick=#{nick},phone=#{phone},is_live=#{isLive} where username=#{username}")
    Boolean updateConsumerNoPassword(@Param("username") String username,@Param("nick") String nick,@Param("phone") String phone,@Param("isLive") Integer isLive);

    @Update("update m_consumer set nick=#{nick},password=#{password},phone=#{phone},is_live=#{isLive} where username=#{username}")
    Boolean updateConsumerAndPassword(@Param("username") String username,@Param("password") String password,@Param("nick") String nick,@Param("phone") String phone,@Param("isLive") Integer isLive);
}
