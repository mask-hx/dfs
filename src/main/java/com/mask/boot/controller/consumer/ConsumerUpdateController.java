package com.mask.boot.controller.consumer;

import com.baomidou.mybatisplus.mapper.EntityWrapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import com.mask.boot.entity.Consumer;
import com.mask.boot.dao.ConsumerMapper;
import com.mask.boot.dao.FileListMapper;
import com.mask.boot.util.MD5Util;
import com.mask.boot.util.ResultEntity;


import java.util.List;

/**
 * ClassName ConsumerUpdateController
 * Create by hx
 * Date 2021/6/4 5:51 下午
 */
@RestController
public class ConsumerUpdateController {
    @Autowired
    private Consumer consumer;
    @Autowired
    private ConsumerMapper consumerMapper;
    @Autowired
    private FileListMapper fileListMapper;

    @RequestMapping("/update/nick")
    public String updateNick(
            @RequestParam("id") Integer id,
            @RequestParam("nick") String nick
    ){
        List<Consumer> list = consumer.selectList(new EntityWrapper()
                .eq("nick", nick)
                .and().ne("id", id)
        );
        if (list.size() == 0){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/update/phone")
    public String updatePhone(
            @RequestParam("id") Integer id,
            @RequestParam("phone") String phone
    ){
        List<Consumer> list = consumer.selectList(new EntityWrapper()
                .eq("phone", phone)
                .and().ne("id", id)
        );
        if (list.size() == 0){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    /**
     * 用户修改信息
     * @param id
     * @param password
     * @param oldNick
     * @param nick
     * @param phone
     * @return
     */
    @RequestMapping("/to/update/consumer")
    public String updateConsumer(
            @RequestParam("id") Integer id,
            @RequestParam("password") String password,
            @RequestParam("oldNick") String oldNick,
            @RequestParam("nick") String nick,
            @RequestParam("phone") String phone
    ){
        consumer.setId(id);
        String md5Password = MD5Util.md5(password);
        consumer.setPassword(md5Password);
        consumer.setNick(nick);
        consumer.setPhone(phone);
        boolean flag = consumer.updateById();
        Boolean updateFileListNick = fileListMapper.updateFileListNick(nick, oldNick);
        if (flag && updateFileListNick){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/get/consumer/detail/info")
    public ResultEntity<List> getConsumerDetailInfo(@RequestParam("username") String username){
        List<Consumer> list = consumer.selectList(new EntityWrapper().eq("username", username));
        if (list.size() == 0){
            return ResultEntity.failed("查询失败");
        }else {
            return ResultEntity.successWithData(list);
        }
    }

    /**
     * 用户忘记密码根据手机号修改
     * @param username
     * @param phone
     * @return
     */
    @RequestMapping("/judge/phone/to/update/password")
    public String judgePhoneToUpdatePassword(
            @RequestParam("username") String username,
            @RequestParam("phone") String phone
    ){
        List<Consumer> list = consumer.selectList(new EntityWrapper()
                .eq("username", username)
                .and().eq("phone", phone)
        );
        if (list.size() == 0){
            return ResultEntity.FAILED;
        }else {
            return ResultEntity.SUCCESS;
        }
    }

    @RequestMapping("/to/update/password")
    public String toUpdatePassword(
            @RequestParam("username") String username,
            @RequestParam("password") String password
    ){
        String md5Password = MD5Util.md5(password);
        Boolean flag = consumerMapper.updatePasswordByUsername(md5Password, username);
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }

    @RequestMapping("/admin/update/consumer")
    public String adminUpdateConsumer(
            String password,
            @RequestParam("username") String username,
            @RequestParam("nick") String nick,
            @RequestParam("phone") String phone,
            @RequestParam("isLive") Integer isLive
    ){
        System.out.println(password);
        //用户没有添加密码
        if (password==null || password.equals("")){
            Boolean flag = consumerMapper.updateConsumerNoPassword(username, nick, phone, isLive);
            if (flag){
                return ResultEntity.SUCCESS;
            }else {
                return ResultEntity.FAILED;
            }
        }else {
            //用户修改密码
            String md5Password = MD5Util.md5(password);
            Boolean flag = consumerMapper.updateConsumerAndPassword(username, md5Password, nick, phone, isLive);
            if (flag){
                return ResultEntity.SUCCESS;
            }else {
                return ResultEntity.FAILED;
            }
        }
    }

    @RequestMapping("/delete/consumer")
    public String deleteConsumer(@RequestParam("id") Integer id){
        consumer.setId(id);
        boolean flag = consumer.deleteById();
        if (flag){
            return ResultEntity.SUCCESS;
        }else {
            return ResultEntity.FAILED;
        }
    }
}
