//用户登录模态框
$("#login").click(function (){
    //显示登录模态框
    getCookie();
    $("#loginModal").modal('show')
})

//用户注册模态框
function modalRegister(){
    //登录模态框隐藏
    $("#loginModal").modal('hide')
    //注册模态框显示
    $("#registerModal").modal('show')

}

//判断输入密码的类型
function CharMode(iN) {
    if (iN >= 48 && iN <= 57) //数字
        return 1;
    if (iN >= 65 && iN <= 90) //大写
        return 2;
    if (iN >= 97 && iN <= 122) //小写
        return 4;
    else
        return 8;
}
//bitTotal函数
//计算密码模式
function bitTotal(num) {
    modes = 0;
    for (i = 0; i < 4; i++) {
        if (num & 1)
            modes++;
        num >>>= 1;
    }
    return modes;
}
//返回强度级别
function checkStrong(sPW) {
    if (sPW.length <= 4)
        return 0; //密码太短
    Modes = 0;
    for (i = 0; i < sPW.length; i++) {
        //密码模式
        Modes |= CharMode(sPW.charCodeAt(i));
    }
    return bitTotal(Modes);
}
//显示颜色
function pwStrength(pwd) {
    O_color = "#eeeeee";
    L_color = "#FF0000";
    M_color = "#FF9900";
    H_color = "#33CC00";
    if (pwd == null || pwd == '') {
        Lcolor = Mcolor = Hcolor = O_color;
    } else {
        $("#password_strong_level").show();
        $("#message_register_password").text('');
        $("#register_password").removeClass("is-invalid")
        S_level = checkStrong(pwd);
        switch (S_level) {
            case 0:
                $("#register_password").removeClass("is-valid")
                Lcolor = Mcolor = Hcolor = O_color;
            case 1:

                $("#register_password").removeClass("is-valid")
                Lcolor = L_color;
                Mcolor = Hcolor = O_color;
                break;
            case 2:

                $("#register_password").removeClass("is-valid")
                Lcolor = Mcolor = M_color;
                Hcolor = O_color;
                break;
            default:
                $("#register_password").addClass("is-valid")
                Lcolor = Mcolor = Hcolor = H_color;
        }
    }
    document.getElementById("strength_L").style.background = Lcolor;
    document.getElementById("strength_M").style.background = Mcolor;
    document.getElementById("strength_H").style.background = Hcolor;
    return;
}

//注册密码判断
$(function (){
    $("#register_password").keyup(function (){
        if ($("#register_password").val() == ''){
            $("#password_strong_level").hide();
            $("#register_password").addClass("is-invalid");
            $("#message_register_password").text("密码不能为空");
        }else {
            $("#register_password").addClass("is-valid")
        }
    })
})

//注册账户名失去焦点判断信息
$(function (){
    $("#register_username").keyup(function (){
        let register_username = $("#register_username");
        if (register_username.val() == ''){
            //账户名为空input边框赋颜色
            register_username.addClass("is-invalid");
            $("#message_register_username").text("账户名不能为空!");
        } else {
            $.ajax({
                url: '/get/username',
                type: 'post',
                data: {
                    register_username:register_username.val()
                },
                datatype: 'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        register_username.removeClass("is-invalid");
                        register_username.addClass("is-valid");
                        $("#message_register_username").text('');
                    }
                    if (data == "FAILED"){
                        register_username.removeClass("is-valid");
                        register_username.addClass("is-invalid");
                        $("#message_register_username").text("账户名已被占用!");
                    }

                }

            })

        }

    })
})

//注册确认密码判断
$(function (){
    $("#register_confirm_password").keyup(function (){
        let registerConfirmPassword = $("#register_confirm_password");
        if (registerConfirmPassword.val() == ''){
            registerConfirmPassword.addClass("is-invalid");
            $("#message_register_confirm_password").text("确认密码不能为空!");
        }else if (registerConfirmPassword.val() != $("#register_password").val()){
            $("#message_register_confirm_password").text("输入两次密码不一致!");
        }else {
            registerConfirmPassword.removeClass("is-invalid");
            registerConfirmPassword.addClass("is-valid");
            $("#message_register_confirm_password").text('');
        }
    })
})

//注册昵称判断
$(function (){
    $("#register_nick").keyup(function (){
        if ($("#register_nick").val() == ''){
            $("#register_nick").addClass("is-invalid");
            $("#message_register_nick").text("昵称不能为空!");
        }else {
            $.ajax({
                url: '/get/nick',
                type: 'post',
                data: {
                    nick:$("#register_nick").val()
                },
                datatype: 'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#register_nick").removeClass("is-invalid");
                        $("#register_nick").addClass("is-valid");
                        $("#message_register_nick").text('');
                    }
                    if (data == "FAILED"){
                        $("#register_nick").removeClass("is-valid");
                        $("#register_nick").addClass("is-invalid");
                        $("#message_register_nick").text("昵称已被占用!");
                    }
                }
            })
        }
    })
})

//短信点击后倒计时
let time = 60;
function getCountDown(){
    if (time === 1){
        time = 60;
        $(".phone-in-input").text("发送验证码")
        $(".phone-in-input").removeAttr("disabled")
        return;
    }else {
        time --;
        $(".phone-in-input").text(time+"s");
        $(".phone-in-input").attr("disabled",true);
    }
    setTimeout(function (){
        getCountDown();
    },1000);
}

//获取短信
function getPhoneMessage(){
    let regex = /^1[34578]\d{9}$/; //手机号正则匹配
    if ($("#register_phone").val() == ''){
        $("#register_phone").addClass("is-invalid")
        $("#message_register_phone").text("请填写手机号!");
    }else if (!regex.test($("#register_phone").val())){
        $("#register_phone").addClass("is-invalid");
        $("#message_register_phone").text("手机号格式不正确")
    }else {
        $.ajax({
            url:'/send/phone',
            type:'get',
            data:{
              phone:$("#register_phone").val()
            },
            datatype:'json',
            success:function (data){
                let result = data.result;
                if (result == "SUCCESS"){
                    layer.msg("发送成功!");
                }
                getCountDown();
                if (result == "FAILED"){
                    layer.msg("请求失败!");
                }

            }
        })
    }

}

//手机号判断
$(function (){
    $("#register_phone").keyup(function (){
        let regex = /^1[34578]\d{9}$/;
        if ($("#register_phone").val() == ''){
            $("#register_phone").addClass("is-invalid")
            $("#message_register_phone").text("请填写手机号!");
        }else if (!regex.test($("#register_phone").val())){
            $("#register_phone").addClass("is-invalid");
            $("#message_register_phone").text("手机号格式有误!")
        }else {
            $.ajax({
                url:'/judge/phone',
                type:'post',
                data:{
                    phone:$("#register_phone").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#message_register_phone").text('');
                        $("#register_phone").removeClass("is-invalid");
                        $("#register_phone").addClass("is-valid");
                    }
                    if (data == "FAILED"){
                        $("#message_register_phone").text("该手机号已被注册!");
                        $("#register_phone").removeClass("is-valid");
                        $("#register_phone").addClass("is-invalid");
                    }
                }
            })
        }
    })



})

//验证码判断
$(function (){
    $("#short_message_input").keyup(function (){
        if ($("#short_message_input").val() == ''){
            $("#message_register_short").text("验证码不能为空!");
        }else {
            $.ajax({
                url:'/get/short/message',
                type:'get',
                data:{
                  phone:$("#register_phone").val(),
                    message:$("#short_message_input").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "codeNotMatch"){
                        $("#message_register_short").text("验证码不正确!")
                        $("#message_register_short_success").text("");
                    }else if (data == "codeTimeout"){
                        $("#message_register_short").text("验证码已过期!");
                        $("#message_register_short_success").text("");
                    }else if (data == "SUCCESS"){
                        $("#message_register_short").text("");
                        $("#message_register_short_success").text("验证码正确!");
                    }else {
                        $("#message_register_short").text("请求超时!");
                        $("#message_register_short_success").text("");
                    }
                }
            })
        }
    })
})

//注册提交
function register(){
    let regex = /^1[34578]\d{9}$/;

    if ($("#register_username").val() == ''){
        layer.msg("账户名不能为空!");
        return;
    }

    if ($("#register_password").val() == ''){
        layer.msg("密码不能为空!");
        return;
    }

    if ($("#register_confirm_password").val() == ''){
        layer.msg("确认密码不能为空!");
        return;
    }

    if ($("#register_password").val() != $("#register_confirm_password").val()){
        layer.msg("两次密码不一致!");
        return;
    }

    if ($("#register_nick").val() == ''){
        layer.msg("昵称不能为空!");
        return;
    }

    if ($("#register_phone").val() == ''){
        layer.msg("手机号不能为空!");
        return;
    }

    if (!regex.test($("#register_phone").val())){
        layer.msg("手机号格式有误!");
        return;
    }

    if ($("#short_message_input").val() == ''){
        layer.msg("验证码不能为空!");
        return;
    }

    let flag1 = 1;
    let flag2 = 1;
    let flag3 = 1;

    $.ajax({
        url: '/get/username',
        type: 'post',
        data: {
            register_username:$("#register_username").val()
        },
        datatype: 'json',
        success:function (data){
            if (data == "FAILED"){
                layer.msg("账户名已被占用!");
                flag1 = 0;
            }
            if (flag1 == 1){
                $.ajax({
                    url: '/get/nick',
                    type: 'post',
                    data: {
                        nick:$("#register_nick").val()
                    },
                    datatype: 'json',
                    success:function (data){
                        if (data == "FAILED"){
                            layer.msg("昵称已被占用!");
                            flag2 = 0;
                        }
                        if (flag2 == 1){
                            $.ajax({
                                url:'/judge/phone',
                                type:'post',
                                data:{
                                    phone:$("#register_phone").val()
                                },
                                datatype:'json',
                                success:function (data){
                                    if (data == "FAILED"){
                                        layer.msg("该手机号已被注册!");
                                        flag3 = 0;
                                    }
                                    if (flag3 == 1){
                                        $.ajax({
                                            url:'/get/short/message',
                                            type:'get',
                                            data:{
                                                phone:$("#register_phone").val(),
                                                message:$("#short_message_input").val()
                                            },
                                            datatype:'json',
                                            success:function (data){
                                                if (data == "codeNotMatch"){
                                                    layer.msg("验证码不正确!");
                                                    return;
                                                }else if (data == "codeTimeout"){
                                                    layer.msg("验证码已过期!")
                                                    return;
                                                }else if (data == "SUCCESS"){
                                                    $.ajax({
                                                        url:'/to/register/consumer',
                                                        type:'post',
                                                        data:{
                                                            username:$("#register_username").val(),
                                                            password:$("#register_password").val(),
                                                            nick:$("#register_nick").val(),
                                                            phone:$("#register_phone").val(),
                                                            isLive:1
                                                        },
                                                        success:function (data){
                                                            if (data == "SUCCESS"){
                                                                $("#registerModal").init();
                                                                $("#registerModal").hide();
                                                                $(".modal-backdrop").remove();
                                                                $("#loginModal").modal("show");
                                                                //注册后的提示
                                                                layer.msg("注册成功!");
                                                            }
                                                            if (data == "FAILED"){
                                                                layer.msg("注册失败!");
                                                            }
                                                        }
                                                    })
                                                }else {
                                                    $("#message_register_short").text("请求超时!");
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}

