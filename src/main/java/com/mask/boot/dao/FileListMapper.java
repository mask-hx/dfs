package com.mask.boot.dao;

import com.baomidou.mybatisplus.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Update;
import com.mask.boot.entity.FileList;

/**
 * ClassName UserMapper
 * Create by hx
 * Date 2021/5/26 8:37 上午
 */
@Mapper
public interface FileListMapper extends BaseMapper<FileList> {

    @Update("UPDATE file_list set file_download_times=file_download_times+1 " +
            "WHERE file_name=#{fileName} AND file_author=#{fileAuthor}")
    Boolean addFileListDownloadTimes(@Param("fileName") String fileName,@Param("fileAuthor") String fileAuthor);

    @Update("update file_list set file_author=#{nick} where file_author=#{oldNick}")
    Boolean updateFileListNick(@Param("nick") String nick,@Param("oldNick") String oldNick);


    @Update("update file_list set file_category_key=#{fileCategoryName} where file_category_key=#{fileCategoryOldName}")
    boolean adminUpdateFileListForCategoryKey(@Param("fileCategoryName") String fileCategoryName,@Param("fileCategoryOldName") String fileCategoryOldName);

}
