package com.mask.boot.entity;

import com.baomidou.mybatisplus.activerecord.Model;
import com.baomidou.mybatisplus.annotations.TableId;
import com.baomidou.mybatisplus.annotations.TableName;
import com.baomidou.mybatisplus.enums.IdType;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;

/**
 * ClassName FileList
 * Create by hx
 * Date 2021/5/29 8:15 下午
 */
@Data
@NoArgsConstructor
@AllArgsConstructor
@TableName("file_list")
@Component
public class FileList extends Model<FileList> {
    @TableId(value = "file_id",type = IdType.AUTO)
    private Integer fileId; //文件id
    private String fileName; //文件名
    private String fileAuthor; //文件所属者
    private Date fileCreateTime; //文件创建时间
    private String fileCategoryKey; //文件所属分类key
    private Integer fileDownloadTimes; //文件下载次数
    private String description; //文件描述


    @Override
    protected Serializable pkVal() {
        return fileId;
    }



}
