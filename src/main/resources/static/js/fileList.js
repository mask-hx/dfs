window.pageIndex = 0;
window.pageSize = 8;
window.dateVal = '';

let tbody = $("#file-tbody");
// let downloadFile = "<button type='button' class='btn btn-primary'>下载</button>"
// let deleteFile = "<button type='button' class='btn btn-danger'>删除</button>"
let previewFile = "<button type='button' class='btn btn-success' onclick='previewFileButton()'>预览</button>"

$(function (){
    //select2组件实现搜索框
    $(".search-select-file-category").select2();

    //layui的日期组件
    laydate.render({
        elem:'#file-date'
    })

    //文件上传判断
    $("#uploadFileButton").click(function (){
        //判断用户是否登录
        if (nick == null){
            layer.msg("请先登录!");
        }else {
            $("#uploadModal").modal("show");
            console.log("username:"+username);
            console.log("nick:"+nick)
        }


    })
})

window.onload = function (){
    //获取用户更新信息
    //updateConsumerDetailInfo()

    //获取用户登录信息
    getSession();
    getCookie();
    //获取文件分类
    getFileCategory();
    //获取用户列表
    getConsumerList()
    //获取文件列表
    generatePageFileList();
}
//页面刷新后加载资源
// angular.module('fileListApp',[])
//     .controller('fileListController',['$scope',function ($scope){
//
//     }])

//1.获取文件分类列表
function getFileCategory(){
    $.ajax({
        url:'/get/file/category',
        type:'get',
        datatype:'json',
        success:function (data){
            let result = data.result;
            if (result == "SUCCESS"){
                let fileCategory = data.data;
                let select = $(".fileCategory");
                for (let i=0;i<fileCategory.length;i++){
                    select.append("<option value='"+fileCategory[i].fileCategoryKey+"'>"+fileCategory[i].fileCategoryName+"</option>")
                }
            }
        }
    })
}
//获取用户列表
function getConsumerList(){
    $.ajax({
        url:'get/consumer/list',
        type:'get',
        datatype:'json',
        success:function (data){
            let result = data.result;
            if (result == "SUCCESS"){
                let consumerList = data.data;
                let select = $("#consumerList");
                for (let i=0;i<consumerList.length;i++){
                    select.append("<option value='"+consumerList[i].id+"'>"+consumerList[i].nick+"</option>")
                }
            }
        }
    })
}


//2.获取文件列表全部数据
function generatePageFileList() {
    // 1.获取分页数据
    var pageInfo = getFileListPageInfo();

    // 2.填充表格
    fillFileListTableBody(pageInfo);
}

//2.1获取文件列表分页数据
function getFileListPageInfo(){
    let ajaxResultFileList = $.ajax({
        url:'/get/file/list',
        type:'get',
        data:{
            pageIndex:pageIndex,
            pageSize:pageSize,
        },
        async:false, //此时的ajax请求必须为同步
        datatype:'json',
    });
    // 判断当前响应状态码是否为200
    let statusCode = ajaxResultFileList.status;
    // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
    if(statusCode != 200) {
        layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResultFileList.statusText);
        return null;
    }
    // 如果响应状态码是200，说明请求处理成功，获取pageInfo
    let resultEntity = ajaxResultFileList.responseJSON;
    // 从resultEntity中获取result属性
    var result = resultEntity.result;

    // 判断result是否成功
    if(result == "FAILED") {
        layer.msg(resultEntity.message);
        return null;
    }

    // 确认result为成功后获取pageInfo
    let pageInfo = resultEntity.data;
    //console.log(pageInfo)

    // 返回pageInfo
    return pageInfo;
}

//2.2填充全部文件列表表格
function fillFileListTableBody(pageInfo){
    // 清除tbody中的旧的内容
    tbody.empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }


    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"

        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileList(pageInfo);
}

//2.3生成全部文件列表导航条
function generateNavigatorFileList(pageInfo){
    // 获取总记录数
    let totalRecord = pageInfo.size;

    // 声明相关属性
    let properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileList,
        "items_per_page": pageSize,
        "current_page": pageIndex-1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}

//2.4全部文件列表翻页的回调函数
function paginationCallBackFileList(pageIndex){
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    generatePageFileList();
    // 取消页码超链接的默认行为
    return false;
}

//文件下载及判断
function downloadFileClick(fileName,fileAuthor){
    var isDownloadSuccess = false;
    if (nick == null || nick == ''){
        layer.msg("请先登录!");
        return;
    }else {
        //window.location.href='/download/file/?filename='+fileName+'&fileAuthor='+fileAuthor;
        window.location.href='https://mask4-boot.oss-cn-shanghai.aliyuncs.com/mask4-file-upload/'+fileName;
        isDownloadSuccess = true;
    }
    if (isDownloadSuccess){
        $.ajax({
            url:'/download/file/success/echo',
            type:'post',
            data:{
                fileName:fileName,
                fileAuthor:fileAuthor
            },
            async:false,
            datatype:'json',
            success:function (data){
                if (data == "SUCCESS"){
                    console.log(data)
                    layer.msg("下载成功!");
                    generatePageFileList();
                }
                if (data == "FAILED"){
                    layer.msg("下载失败!请稍后再试");
                }
            }
        })
    }

}

//文件删除及权限控制
function deleteFileClick(fileName,fileAuthor,fileId,fileCategoryKey){
    if (nick == null){
        layer.msg("请先登录！");
        return;
    }
    if (nick != fileAuthor){
        layer.msg("您没有删除该文件的权限");
        return;
    }
    if (nick == fileAuthor){

        layer.confirm("尊敬的"+fileAuthor+"！确认删除 "+fileName+" 该文件吗?",function (){
            $.ajax({
                url: '/to/delete/file',
                type: 'post',
                async: false,
                data: {
                    fileId: fileId,
                    fileCategoryKey: fileCategoryKey,
                    fileName: fileName
                },
                datatype: 'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        layer.msg("删除成功!");
                        generatePageFileList();
                    }
                    if (data == "FAILED"){
                        layer.msg("请求超时!请稍后再试");
                    }
                }
            })
        })
    }
}


//3.根据文件分类查询文件 onchange函数

function searchByFileCategory(){
    let optionText = $("#searchByFileCategory option:selected");
    if (nick == null && optionText.text() == "私有文件"){
        layer.msg("请先登录!");
        return;
    } else {
        generatePageCategoryFile();
    }

}





//3.1获取文件分类列表
function generatePageCategoryFile() {
    // 1.获取分页数据
    var pageInfo = getFileCategoryPageInfo();

    // 2.填充表格
    fillFileCategoryTableBody(pageInfo);
}

//3.2获取文件分类的pageInfo数据
function getFileCategoryPageInfo(){
    //获取option选中的value
    let optionVal = $("#searchByFileCategory option:selected").val();
    //获取option选中的文本
    let optionText = $("#searchByFileCategory option:selected").text();
    if (optionVal != ''){
        let ajaxResult = $.ajax({
            url:'/get/file/list/by/category',
            type:'post',
            async:false,
            data:{
                pageIndex:pageIndex,
                pageSize:pageSize,
                categoryName: optionText,
                fileAuthor:nick
            },
            datatype:'json',
        });

        // 判断当前响应状态码是否为200
        let statusCode = ajaxResult.status;
        // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
        if(statusCode != 200) {
            layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResult.statusText);
            return null;
        }
        // 如果响应状态码是200，说明请求处理成功，获取pageInfo
        let resultEntity = ajaxResult.responseJSON;
        // 从resultEntity中获取result属性
        var result = resultEntity.result;

        // 判断result是否成功
        if(result == "FAILED") {
            layer.msg(resultEntity.message);
            return null;
        }

        // 确认result为成功后获取pageInfo
        let pageInfo = resultEntity.data;
        //console.log(pageInfo)

        // 返回pageInfo
        return pageInfo;


    }
}
//3.3填充文件分类表格
function fillFileCategoryTableBody(pageInfo){
    // 清除tbody中的旧的内容
    $("#file-tbody").empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }

    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"
        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileCategory(pageInfo);

}

//3.4 生成文件分类导航条
function generateNavigatorFileCategory(pageInfo){
    // 获取总记录数
    var totalRecord = pageInfo.size;

    // 声明相关属性
    var properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileCategory,
        "items_per_page": pageSize,
        "current_page": pageIndex - 1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}
//3.5文件分类翻页时的回调函数
function paginationCallBackFileCategory(pageIndex) {
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    generatePageCategoryFile();
    // 取消页码超链接的默认行为
    return false;
}
//根据用户查询文件列表
function searchFileListByAuthor(){
    generateAuthorFileList();
}

function generateAuthorFileList(){
    let pageInfo = getAuthorFilePageInfo();

    fillFileAuthorListTableBody(pageInfo);
}

function getAuthorFilePageInfo(){
    let val = $("#consumerList option:selected").val();
    let fileAuthor = $("#consumerList option:selected").text();
    if (fileAuthor != ''){
        let ajaxResult = $.ajax({
            url:'/get/file/author/list',
            type:'post',
            async:false,
            data:{
                pageIndex:pageIndex,
                pageSize:pageSize,
                fileAuthor: fileAuthor
            },
            datatype:'json',
        });

        // 判断当前响应状态码是否为200
        let statusCode = ajaxResult.status;
        // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
        if(statusCode != 200) {
            layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResult.statusText);
            return null;
        }
        // 如果响应状态码是200，说明请求处理成功，获取pageInfo
        let resultEntity = ajaxResult.responseJSON;
        // 从resultEntity中获取result属性
        var result = resultEntity.result;

        // 判断result是否成功
        if(result == "FAILED") {
            layer.msg(resultEntity.message);
            return null;
        }

        // 确认result为成功后获取pageInfo
        let pageInfo = resultEntity.data;
        //console.log(pageInfo)

        // 返回pageInfo
        return pageInfo;
    }
}

function fillFileAuthorListTableBody(pageInfo){
    // 清除tbody中的旧的内容
    $("#file-tbody").empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }
    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"
        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileAuthor(pageInfo);
}

function generateNavigatorFileAuthor(pageInfo){
    // 获取总记录数
    var totalRecord = pageInfo.size;

    // 声明相关属性
    var properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileAuthor,
        "items_per_page": pageSize,
        "current_page": pageIndex - 1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}

function paginationCallBackFileAuthor(pageIndex){
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    generateAuthorFileList();
    // 取消页码超链接的默认行为
    return false;
}


laydate.render({
    elem: '#file-date', //指定元素
    isPreview: true, //左下角预览日期
    lang: 'cn',
    done: function(value){
        dateVal = value
        generateFileListByDate();
    }
});

function getFileListByDate(){
    let ajaxResult = $.ajax({
        url:'/get/file/list/by/date',
        type:'get',
        data:{
            pageIndex:pageIndex,
            pageSize:pageSize,
            date:dateVal
        },
        async:false,
        datatype:'json',
    });
    // 判断当前响应状态码是否为200
    let statusCode = ajaxResult.status;
    // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
    if(statusCode != 200) {
        layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResult.statusText);
        return null;
    }
    // 如果响应状态码是200，说明请求处理成功，获取pageInfo
    let resultEntity = ajaxResult.responseJSON;
    // 从resultEntity中获取result属性
    var result = resultEntity.result;

    // 判断result是否成功
    if(result == "FAILED") {
        layer.msg(resultEntity.message);
        return null;
    }

    // 确认result为成功后获取pageInfo
    let pageInfo = resultEntity.data;
    //console.log(pageInfo)

    // 返回pageInfo
    return pageInfo;
}

function generateFileListByDate(){
    let pageInfo = getFileListByDate();
    fillFileListByDateTableBody(pageInfo)
}

function fillFileListByDateTableBody(pageInfo){
    // 清除tbody中的旧的内容
    $("#file-tbody").empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }
    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"
        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileByDate(pageInfo);
}

function generateNavigatorFileByDate(pageInfo){
    // 获取总记录数
    var totalRecord = pageInfo.size;

    // 声明相关属性
    var properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileByDate,
        "items_per_page": pageSize,
        "current_page": pageIndex - 1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}

function paginationCallBackFileByDate(pageIndex){
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    generateFileListByDate();
    // 取消页码超链接的默认行为
    return false;
}

//文件上传表单异步请求
$(document).ready(function () {
    $("#uploadForm").ajaxForm(function () {

    });
});
//文件上传
function uploadFile() {
    var fileVal = $("#icon").val();
    var categoryKey = $("#categoryKey option:selected").val();
    var iconSize = document.getElementById('icon').files[0];
    var fileMaxSize = 200 * 1024 * 1024;  //文件最大为200M
    if (fileVal == "") {
        layer.msg("请添加上传文件!");
        return;
    }
    if (categoryKey == "") {
        layer.msg("请选择上传文件分类!");
        return;
    }
    if (iconSize.size > fileMaxSize) {
        console.log(iconSize.size);
        //console.log("test");
        layer.msg("上传文件不得大于200M");
        return;
    }else {
        let formData = new FormData();
        formData.append("icon",$("#icon")[0].files[0]);
        formData.append("categoryKey",$("#categoryKey option:selected").text());
        formData.append("description",$("#description").val());
        formData.append("fileAuthor",nick);
        $.ajax({
            type:'post',
            enctype:'multipart/form-data',
            url: '/upload',
            data:formData,
            cache:false,
            processData:false,
            contentType:false,
            async:false,
            success:function (data){
                console.log(data);

            },
            xhr: function () {
                var xhr = $.ajaxSettings.xhr();
                if (xhr.upload) {
                    $("#icon").attr("disabled",true);
                    $("#uploadFormData").attr("disabled",true);
                    //处理进度条的事件
                    $("#uploadModal").modal('show');

                    xhr.upload.addEventListener("progress", progressHandle, false);
                    //加载完成的事件
                    xhr.addEventListener("load", completeHandle, true);
                    //加载出错的事件
                    xhr.addEventListener("error", failedHandle, true);
                    //加载取消的事件
                    xhr.addEventListener("abort", canceledHandle, true);
                    //开始显示进度条
                    showProgress(e);
                    return xhr;
                }
            }
        },'json')
    }
}
var start = 0;
//显示进度条的函数
function showProgress() {
    start = new Date().getTime();
    $(".progress-body").css("display", "block");
    $('.progress-body .progress-info').html("上传文件中...");
    //点击上传进度条和文字显示
    $(".progress-body progress").show();
    $(".progress-body percentage").show();


    $("#uploadModal").modal('show');
}
//隐藏进度条的函数
function hideProgress() {
    $("#uploadFormData").val('');
    $('.progress-body .progress-speed').html("0 M/S, 0/0M");
    $('.progress-body percentage').html("0%");
    $('.progress-body .progress-info').html("请选择文件并点击上传文件按钮");
    $("#icon").attr("disabled",false);
    $("#uploadFormData").attr("disabled",false);
    $(".progress-body").css("display", "none");
}
//进度条更新
function progressHandle(e) {
    //点击上传进度条和文字显示
    $(".progress-body progress").show();
    $(".progress-body percentage").show();

    //显示进度百分数
    $('.progress-body progress').attr({value: e.loaded, max: e.total});
    //计算进度数
    var percent = e.loaded / e.total * 100;
    //需要根据网速计算需要花费的时间
    var time = ((new Date().getTime() - start) / 1000).toFixed(3);
    if(time == 0){
        time = 1;
    }
    //显示上传速度，和上传文件大小及进度
    $('.progress-body .progress-speed').html(((e.loaded / 1024) / 1024 / time).toFixed(2) + "M/S, " + ((e.loaded / 1024) / 1024).toFixed(2) + "/" + ((e.total / 1024) / 1024).toFixed(2) + " MB. ");
    //更新上传进度数
    $('.progress-body percentage').html(percent.toFixed(2) + "%");
    //更新进度条宽度
    $(".progress-body progress").width(percent.toFixed(0));
    if (percent == 100) {
        $('.progress-body .progress-info').html("上传完成,后台正在处理...");
    } else {
        $('.progress-body .progress-info').html("文件上传中...");
    }
};
//上传完成处理函数
function completeHandle(e) {
    //$('.progress-body .progress-info').html("上传文件中...");
    setTimeout(hideProgress, 2000);
    $("#uploadModal").modal('hide');
    layer.msg("上传成功!");
    generatePageFileList();

};
//上传出错处理函数
function failedHandle(e) {
    $('.progress-body .progress-info').html("上传文件出错, 服务不可用或文件过大。");
    setTimeout(hideProgress, 2000);
};
//上传取消处理函数
function canceledHandle(e) {
    $('.progress-body .progress-info').html("上传文件取消。");
    setTimeout(hideProgress, 2000);
};


//根据所有文件分类、文件所属者、文件时间条件查询文件列表
function searchFileListByAllCondition(){
    let pageInfo = searchAllCondition();
    //如果没有查询条件则不清空表格
    if (pageInfo!= undefined){
        fillFileListByAllConditionTableBody(pageInfo)
    }
}

function searchAllCondition(){
    let fileCategory = $("#searchByFileCategory option:selected").text();
    let fileAuthor = $("#consumerList option:selected").text();
    if (fileCategory!='' && fileAuthor!='' && dateVal!=''){
        let ajaxResult = $.ajax({
            url:'/get/file/list/all/condition',
            type:'get',
            data:{
                pageIndex:pageIndex,
                pageSize:pageSize,
                fileCategory:fileCategory,
                fileAuthor:fileAuthor,
                fileCreateTime:dateVal
            },
            async:false,
            datatype:'json',
        });
        // 判断当前响应状态码是否为200
        let statusCode = ajaxResult.status;
        // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
        if(statusCode != 200) {
            layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResult.statusText);
            return null;
        }
        // 如果响应状态码是200，说明请求处理成功，获取pageInfo
        let resultEntity = ajaxResult.responseJSON;
        // 从resultEntity中获取result属性
        var result = resultEntity.result;

        // 判断result是否成功
        if(result == "FAILED") {
            layer.msg(resultEntity.message);
            return null;
        }

        // 确认result为成功后获取pageInfo
        let pageInfo = resultEntity.data;
        //console.log(pageInfo)

        // 返回pageInfo
        return pageInfo;
    }else {
        layer.msg("请选择查询条件!");
        return;
    }
}

function fillFileListByAllConditionTableBody(pageInfo){
    // 清除tbody中的旧的内容
    $("#file-tbody").empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }
    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"
        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileAllCondition(pageInfo);
}

function generateNavigatorFileAllCondition(pageInfo){
    // 获取总记录数
    var totalRecord = pageInfo.size;

    // 声明相关属性
    var properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileByAllCondition,
        "items_per_page": pageSize,
        "current_page": pageIndex - 1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}

function paginationCallBackFileByAllCondition(pageIndex){
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    searchFileListByAllCondition();
    // 取消页码超链接的默认行为
    return false;
}

//关键字查询文件列表
function searchByKeyword(){
    let pageInfo = generatePageInfoByKeyword();
    if (pageInfo != undefined){
        fillFileListByKeywordTableBody(pageInfo)
    }
}

function generatePageInfoByKeyword(){
    let keyword = $("#keyword").val();
    if (keyword != ''){
        let ajaxResult = $.ajax({
            url:'/get/file/list/by/keyword',
            type:'get',
            data:{
                pageIndex:pageIndex,
                pageSize:pageSize,
                keyword:keyword
            },
            async:false,
            datatype:'json',
        });
        // 判断当前响应状态码是否为200
        let statusCode = ajaxResult.status;
        // 如果当前响应状态码不是200，说明发生了错误或其他意外情况，显示提示消息，让当前函数停止执行
        if(statusCode != 200) {
            layer.msg("失败！响应状态码="+statusCode+" 说明信息="+ajaxResult.statusText);
            return null;
        }
        // 如果响应状态码是200，说明请求处理成功，获取pageInfo
        let resultEntity = ajaxResult.responseJSON;
        // 从resultEntity中获取result属性
        var result = resultEntity.result;

        // 判断result是否成功
        if(result == "FAILED") {
            layer.msg(resultEntity.message);
            return null;
        }

        // 确认result为成功后获取pageInfo
        let pageInfo = resultEntity.data;
        //console.log(pageInfo)

        // 返回pageInfo
        return pageInfo;
    }else {
        layer.msg("请输入查询关键字!");
        return;
    }

}

function fillFileListByKeywordTableBody(pageInfo){
    // 清除tbody中的旧的内容
    $("#file-tbody").empty();

    // 这里清空是为了让没有搜索结果时不显示页码导航条
    $("#Pagination").empty();

    // 判断pageInfo对象是否有效
    if(pageInfo == null || pageInfo == undefined || pageInfo.size == null || pageInfo.size == 0) {
        $("#file-tbody").append("<tr><td colspan='7'><p>抱歉！没有查询到您搜索的数据!</p></td></tr>")
        return ;
    }
    let fileList = pageInfo.records;
    for (let i=0;i<fileList.length;i++){
        let number = "<td>"+(i+1)+"</td>";
        let fileName = "<td>"+fileList[i].fileName+"</td>";
        let fileCategoryKey = "<td>"+fileList[i].fileCategoryKey+"</td>";
        let fileAuthor = "<td>"+fileList[i].fileAuthor+"</td>";
        let dateTime = "<td>"+fileList[i].fileCreateTime.substring(0,10)+"</td>"
        let fileDownloadTimes = "<td>"+fileList[i].fileDownloadTimes+"</td>"
        let downloadFile = "<a class='btn btn-primary' type='button' onclick=downloadFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"')>下载</a>"
        let deleteFile = "<a class='btn btn-danger' type='button' onclick=deleteFileClick('"+fileList[i].fileName+"','"+fileList[i].fileAuthor+"','"+fileList[i].fileId+"','"+fileList[i].fileCategoryKey+"')>删除</a>"
        let operate = "<td>"+previewFile+downloadFile+deleteFile+"</td>"
        let tr = "<tr data-widget='expandable-table' aria-expanded='false'>"+
            number+fileName+fileCategoryKey+fileAuthor+dateTime+fileDownloadTimes
            +operate+"</tr>"
        tbody.append(tr);
        tbody.append("<tr class='expandable-body d-none'>" +
            "<td colspan='7'><p style='display: none;'>"+"文件描述："+fileList[i].description+"</p></td>")
    }
    // 生成分页导航条
    generateNavigatorFileListByKeyword(pageInfo);
}

function generateNavigatorFileListByKeyword(pageInfo){
    // 获取总记录数
    var totalRecord = pageInfo.size;

    // 声明相关属性
    var properties = {
        "num_edge_entries": 3,
        "num_display_entries": 5,
        "callback": paginationCallBackFileListByKeyword,
        "items_per_page": pageSize,
        "current_page": pageIndex - 1,
        "prev_text": "上一页",
        "next_text": "下一页"
    };

    // 调用pagination()函数
    $("#Pagination").pagination(totalRecord, properties);
}
function paginationCallBackFileListByKeyword(pageIndex){
    // 修改window对象的pageNum属性
    window.pageIndex = pageIndex + 1;
    // 调用分页函数
    searchByKeyword();
    // 取消页码超链接的默认行为
    return false;
}

//键盘按下回车执行关键字查询操作
document.onkeydown = function (event) {
    e = event ? event : (window.event ? window.event : null);
    if (e.keyCode == 13 && $("#keyword").val()!='') {
        searchByKeyword();
    }
    if (e.keyCode == 13 && $("#login_username").val() != ''){
        login();
    }

}

function previewFileButton(){
    layer.msg("您没有预览权限,请向管理员申请");
    return;
}



