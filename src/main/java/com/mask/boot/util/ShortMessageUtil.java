package com.mask.boot.util;

import org.apache.http.HttpResponse;
import org.apache.http.StatusLine;
import org.apache.http.util.EntityUtils;

import java.util.HashMap;
import java.util.Map;

/**
 * ClassName ShortMessageUtil
 * Create by hx
 * Date 2021/5/23 1:43 下午
 */

public class ShortMessageUtil {
    /**
     * 发送短信验证码
     * @param phone 接收短信的手机号
     * @return 结果信息和验证码
     */
    public static ResultEntity<String> sendCodeByShortMessage(String phone){
        String host = "https://dfsns.market.alicloudapi.com";
        String path = "/data/send_sms";
        String method = "POST";
        String appcode = "03cfc44b348242188154f2af5ded9c40";
        Map<String, String> headers = new HashMap<String, String>();
        //生成6位数验证码
        StringBuilder builder = new StringBuilder();
        for (int i = 0; i < 6; i++) {
            int random = (int) (Math.random() * 10);
            builder.append(random);
        }
        String code = builder.toString();


        //最后在header中的格式(中间是英文空格)为Authorization:APPCODE 83359fd73fe94948385f570e3c139105
        headers.put("Authorization", "APPCODE " + appcode);
        //根据API的要求，定义相对应的Content-Type
        headers.put("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
        Map<String, String> querys = new HashMap<String, String>();
        Map<String, String> bodys = new HashMap<String, String>();
        bodys.put("content", "code:"+code+",time:1");
        //收短信的手机号
        bodys.put("phone_number", phone);
        //模版编号
        //默认模版
        //bodys.put("template_id", "TPL_0001");
        //短信用户登录模版
        //bodys.put("template_id", "MASK_0001");
        //短信修改用户模版
        bodys.put("template_id", "MASK_0003");

        try {
            /**
             * 重要提示如下:
             * HttpUtils请从
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/src/main/java/com/aliyun/api/gateway/demo/util/HttpUtils.java
             * 下载
             * 相应的依赖请参照
             * https://github.com/aliyun/api-gateway-demo-sign-java/blob/master/pom.xml
             */
            HttpResponse response = HttpUtils.doPost(host, path, method, headers, querys, bodys);
            StatusLine statusLine = response.getStatusLine();
            //响应码200正常,400验证码错误,403调用次数用完,500服务器内部错误
            int status = statusLine.getStatusCode();
            String request_id = statusLine.getReasonPhrase();
            //验证码正确
            if (status == 200){
                return ResultEntity.successWithData(code);
            }
            //验证码错误，返回状态码
            return ResultEntity.failed(EntityUtils.toString(response.getEntity()));
            //System.out.println(response.toString());
            //System.out.println("status:"+status);
            //System.out.println("request_id:"+request_id);
            //{
            // "status":"INVALID_ARGUMENT",
            // "reason":"invalid phone number",
            // "request_id":"TIDaa82dba9b8974301a3121248ca26ed9c"}
            //System.out.println(EntityUtils.toString(response.getEntity()));
            //获取response的body
            //System.out.println(EntityUtils.toString(response.getEntity()));
        } catch (Exception e) {
            e.printStackTrace();
            return ResultEntity.failed(e.getMessage());
        }
    }

}
