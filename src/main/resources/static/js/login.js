window.id = null;
window.username = null;
window.nick = null;
window.phone = null;
window.registerTime = null;
//登录输入框失去焦点判断
$(function (){
    $("#login_username").keyup(function (){
        if ($("#login_username").val() == ''){
            $("#message_login_username").text("账户名不能为空!");
            $("#login_username").addClass("is-invalid");
        }else {
            $.ajax({
                url:'/to/login/judge/username',
                type:'post',
                data:{
                    username:$("#login_username").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#message_login_username").text("");
                        $("#login_username").removeClass("is-invalid");
                        $("#login_username").addClass("is-valid");
                    }
                    if (data == "FAILED"){
                        $("#message_login_username").text("账户名不存在");
                        $("#login_username").removeClass("is-valid");
                        $("#login_username").addClass("is-invalid");
                    }
                }

            })
        }
    })
    $("#login_password").keyup(function (){
        if ($("#login_password").val() == ''){
            $("#message_login_password").text("密码不能为空!");
            $("#login_password").addClass("is-invalid");
        }else {
            $("#message_login_password").text("");
            $("#login_password").removeClass("is-invalid");
        }
    })


})


//用户登录执行
function login(){
    if ($("#login_username").val() == ''){
        layer.msg("账户不能为空!");
        return;
    }
    if ($("#login_password").val() == ''){
        layer.msg("密码不能为空!");
        return;
    }
    let rememberMe = false;
    if ($("#rememberMe").is(":checked")){
        rememberMe = true;
    }
    $.ajax({
        url: '/to/login',
        type: 'post',
        data:{
            username:$("#login_username").val(),
            password:$("#login_password").val(),
            rememberMe:rememberMe
        },
        statusCode:{
            700:function (){
                layer.msg("该账户已被禁用,请联系管理员");
            },
            500:function (){
                layer.msg("账户名和密码不匹配!");
            }
        },
        success:function (data){
            if (data == "SUCCESS"){
                $("#loginModal").modal("hide");

                //登录成功后手动清空登录模态框中的所有值
                $("#loginModal").init();
                $("#login_username").val('');
                $("#login_password").val('');
                $("#rememberMe").attr("checked",false);
                $("#login_username").removeClass("is-invalid");
                $("#login_username").removeClass("is-valid");
                $("#login_password").removeClass("is-invalid","is-valid");
                $("#message_login_username").text('');
                $("#message_login_password").text('');

                layer.msg("登录成功!");
                getCookie();
                getSession();
                generatePageFileList();

            }
            if (data == "FAILED"){
                layer.msg("账户名和密码不匹配");
            }
        }
    })

}

////获取用户cookie信息
function getCookie(){
    $.ajax({
        url:'/get/consumer/cookie',
        type:'get',
        datatype: 'json',
        success:function (data){
            let result = data.result;
            if (result == "SUCCESS"){
                let cookieJson = JSON.stringify(data.data);
                let array = cookieJson.split(/{|"|,|:|}/);
                for (let i=0;i<array.length;i++){
                    if (array[i] == "username"){
                        //获取用户名添加到用户输入框
                        let username = array[i+3];
                        $("#login_username").val(username);
                    }

                    if (array[i] == "password"){
                        //获取用户密码添加到输入框
                        let password = array[i+3];
                        $("#login_password").val(password);
                    }

                }
            }
            // //如果不勾选记住则清除输入框和勾选状态
            // if (result == "FAILED"){
            //     $("#login_username").val('');
            //     $("#login_password").val();
            //     $("#rememberMe").removeAttr('checked');
            // }
        }
    })
}


//获取用户session信息
function getSession(){

    $.ajax({
        url:'/get/consumer/session',
        type:'get',
        datatype:'json',
        success:function (data){
            let result = data.result;
            if (result == "SUCCESS"){
                let list = data.data;
                window.id = list[0].id;
                window.username = list[0].username;
                window.nick = list[0].nick;
                window.phone = list[0].phone;
                window.registerTime = list[0].registerTime.substr(0,10)
                $("#login").text('');
                $("#alreadyLogin").text("欢迎您,"+nick);
                $("#logout").append("<img src='/images/logout.png' class='logout-img' alt='退出登录'>")
            }

        }
    })
}

//页面刷新加载资源
window.onload = function (){
    getSession();
    getCookie();
}

//退出登录
$(function (){
    $("#logout").click(function (){
        // if (){
        //     layer.msg("注销成功!");
        // }
        layer.confirm("尊敬的"+nick+"！确认退出登录吗?",function (){
            $.ajax({
                url:'/to/logout',
                type:'get',
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#login").text("登录/注册");
                        $("#alreadyLogin").text('');
                        $("#logout").empty();
                        //退出登录后清空用户session
                        username = null;
                        nick = null;
                        // //登录成功后手动清空登录模态框中的所有值
                        $("#login_username").val('');
                        $("#login_password").val('');
                        $("#rememberMe").removeAttr('checked');
                        $("#login_username").removeClass("is-invalid","is-valid");
                        $("#login_password").removeClass("is-invalid","is-valid");
                        $("#message_login_username").text('');
                        $("#message_login_password").text('');
                        layer.msg("注销成功!");
                    }
                    if (data == "FAILED"){
                        layer.msg("注销失败!");
                    }
                }
            })

        })
    })
})

//首页点击个人中心判断
function toPersonCenter(){
    if (nick == null){
        layer.msg("请先登录!");
        return;
    }else {
        window.location.href='./personCenter.html';

    }
}

//忘记密码模态框展示
function foreignPassword(){
    $("#loginModal").modal("hide");
    $("#foreignPasswordModal").modal("show")
}

//忘记密码输入框keyup判断
$(function (){
    let foreignUsername = $("#foreign_username");
    let messageForeignUsername = $("#message_foreign_username");
    let foreignPassword = $("#foreign_password");
    let foreignConfirmPassword = $("#foreign_confirm_password");
    let foreignPhone = $("#foreign_phone");

    //账户名keyup判断
    foreignUsername.keyup(function (){
        if (foreignUsername.val() == ''){
            messageForeignUsername.text("请填写用户名!");
            foreignUsername.addClass("is-invalid");
        }else {
            $.ajax({
                url:'/to/login/judge/username',
                type:'post',
                data:{
                    username:foreignUsername.val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        messageForeignUsername.text("");
                        foreignUsername.removeClass("is-invalid");
                        foreignUsername.addClass("is-valid");
                    }
                    if (data == "FAILED"){
                        messageForeignUsername.text("账户名不存在");
                        foreignUsername.removeClass("is-valid");
                        foreignUsername.addClass("is-invalid");
                    }
                }

            })
        }
    })

    //密码keyup判断
    foreignPassword.keyup(function (){
        if (foreignPassword.val() == ''){
            $("#foreign_password_strong_level").hide();
            foreignPassword.addClass("is-invalid");
            $("#message_foreign_password").text("密码不能为空");
        }else {
            foreignPassword.addClass("is-valid")
        }
    })

    //更新密码确认密码判断
    foreignConfirmPassword.keyup(function (){
        if (foreignConfirmPassword.val() == ''){
            foreignConfirmPassword.addClass("is-invalid");
            $("#message_foreign_confirm_password").text("确认密码不能为空!");
        }else if (foreignConfirmPassword.val() != foreignPassword.val()){
            $("#message_foreign_confirm_password").text("输入两次密码不一致!");
        }else {
            foreignConfirmPassword.removeClass("is-invalid");
            foreignConfirmPassword.addClass("is-valid");
            $("#message_foreign_confirm_password").text('');
        }
    })

    //更新密码手机号判断
    foreignPhone.keyup(function (){
        let regex = /^1[34578]\d{9}$/;
        if (foreignPhone.val() == ''){
            foreignPhone.addClass("is-invalid")
            $("#message_foreign_phone").text("请填写手机号!");
        }else if (!regex.test(foreignPhone.val())){
            foreignPhone.addClass("is-invalid");
            $("#message_foreign_phone").text("手机号格式有误!")
        }else {
            $.ajax({
                url:'/judge/phone/to/update/password',
                type:'post',
                data:{
                    username:$("#foreign_username").val(),
                    phone:foreignPhone.val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "SUCCESS"){
                        $("#message_foreign_phone").text('');
                        foreignPhone.removeClass("is-invalid");
                        foreignPhone.addClass("is-valid");
                    }
                    if (data == "FAILED"){
                        $("#message_foreign_phone").text("该手机号不属于该用户!");
                        foreignPhone.removeClass("is-valid");
                        foreignPhone.addClass("is-invalid");
                    }
                }
            })
        }
    })

    //验证码keyup判断
    $("#foreign_short_message_input").keyup(function (){
        if ($("#foreign_short_message_input").val() == ''){
            $("#foreign_message_short").text("验证码不能为空!");
        }else {
            $.ajax({
                url:'/get/short/message',
                type:'get',
                data:{
                    phone:$("#foreign_phone").val(),
                    message:$("#foreign_short_message_input").val()
                },
                datatype:'json',
                success:function (data){
                    if (data == "codeNotMatch"){
                        $("#foreign_message_short").text("验证码不正确!")
                        $("#foreign_message_short_success").text("");
                    }else if (data == "codeTimeout"){
                        $("#foreign_message_short").text("验证码已过期!");
                        $("#foreign_message_short_success").text("");
                    }else if (data == "SUCCESS"){
                        $("#foreign_message_short").text("");
                        $("#foreign_message_short_success").text("验证码正确!");
                    }else {
                        $("#foreign_message_short").text("请求超时!");
                        $("#foreign_message_short_success").text("");
                    }
                }
            })
        }
    })

})

//判断输入密码的类型
function CharModeForeign(iN) {
    if (iN >= 48 && iN <= 57) //数字
        return 1;
    if (iN >= 65 && iN <= 90) //大写
        return 2;
    if (iN >= 97 && iN <= 122) //小写
        return 4;
    else
        return 8;
}
//bitTotal函数
//计算密码模式
function bitTotalForeign(num) {
    modes = 0;
    for (i = 0; i < 4; i++) {
        if (num & 1)
            modes++;
        num >>>= 1;
    }
    return modes;
}
//返回强度级别
function checkStrongForeign(sPW) {
    if (sPW.length <= 4)
        return 0; //密码太短
    Modes = 0;
    for (i = 0; i < sPW.length; i++) {
        //密码模式
        Modes |= CharModeForeign(sPW.charCodeAt(i));
    }
    return bitTotalForeign(Modes);
}
//显示颜色
function pwStrengthForeign(pwd) {
    O_color = "#eeeeee";
    L_color = "#FF0000";
    M_color = "#FF9900";
    H_color = "#33CC00";
    if (pwd == null || pwd == '') {
        Lcolor = Mcolor = Hcolor = O_color;
    } else {
        $("#foreign_password_strong_level").show();
        $("#message_foreign_password").text('');
        $("#foreign_password").removeClass("is-invalid")
        S_level = checkStrongForeign(pwd);
        switch (S_level) {
            case 0:
                $("#foreign_password").removeClass("is-valid")
                Lcolor = Mcolor = Hcolor = O_color;
            case 1:

                $("#foreign_password").removeClass("is-valid")
                Lcolor = L_color;
                Mcolor = Hcolor = O_color;
                break;
            case 2:

                $("#foreign_password").removeClass("is-valid")
                Lcolor = Mcolor = M_color;
                Hcolor = O_color;
                break;
            default:
                $("#foreign_password").addClass("is-valid")
                Lcolor = Mcolor = Hcolor = H_color;
        }
    }
    document.getElementById("foreign_strength_L").style.background = Lcolor;
    document.getElementById("foreign_strength_M").style.background = Mcolor;
    document.getElementById("foreign_strength_H").style.background = Hcolor;
    return;
}

//短信点击后倒计时
let timeForeign = 60;
function getCountDownForeign(){
    if (timeForeign === 1){
        timeForeign = 60;
        $(".foreign-phone-in-input").text("发送验证码")
        $(".foreign-phone-in-input").removeAttr("disabled")
        return;
    }else {
        timeForeign --;
        $(".foreign-phone-in-input").text(timeForeign+"s");
        $(".foreign-phone-in-input").attr("disabled",true);
    }
    setTimeout(function (){
        getCountDownForeign();
    },1000);
}

//获取短信
function getPhoneMessageForeign(){
    let regex = /^1[34578]\d{9}$/;
    if ($("#foreign_phone").val() == ''){
        $("#foreign_phone").addClass("is-invalid")
        $("#message_foreign_phone").text("请填写手机号!");
    }else if (!regex.test($("#foreign_phone").val())){
        $("#foreign_phone").addClass("is-invalid");
        $("#message_foreign_phone").text("手机号格式不正确")
    }else {
        $.ajax({
            url:'/send/phone',
            type:'get',
            data:{
                phone:$("#foreign_phone").val()
            },
            datatype:'json',
            success:function (data){
                let result = data.result;
                if (result == "SUCCESS"){
                    layer.msg("发送成功!");
                }
                getCountDownForeign();
                if (result == "FAILED"){
                    layer.msg("请求失败!");
                }
            }
        })
    }

}

//更新密码提交
function updatePersonPassword(){
    let regex = /^1[34578]\d{9}$/;

    if ($("#foreign_username").val() == ''){
        layer.msg("账户名不能为空!");
        return;
    }

    if ($("#foreign_password").val() == ''){
        layer.msg("密码不能为空!");
        return;
    }

    if ($("#foreign_confirm_password").val() == ''){
        layer.msg("确认密码不能为空!");
        return;
    }

    if ($("#foreign_password").val() != $("#foreign_confirm_password").val()){
        layer.msg("两次密码不一致!");
        return;
    }

    if ($("#foreign_phone").val() == ''){
        layer.msg("手机号不能为空!");
        return;
    }

    if (!regex.test($("#foreign_phone").val())){
        layer.msg("手机号格式有误!");
        return;
    }

    if ($("#foreign_short_message_input").val() == ''){
        layer.msg("验证码不能为空!");
        return;
    }

    let flag1 = 1;
    let flag2 = 1;
    let flag3 = 1;

    $.ajax({
        url: '/to/login/judge/username',
        type: 'post',
        data: {
            username:$("#foreign_username").val()
        },
        datatype: 'json',
        success:function (data){
            if (data == "FAILED"){
                layer.msg("账户名不存在!");
                flag1 = 0;
            }
            if (flag1 == 1){
                $.ajax({
                    url: '/get/nick',
                    type: 'post',
                    data: {
                        nick:$("#register_nick").val()
                    },
                    datatype: 'json',
                    success:function (data){
                        // if (data == "FAILED"){
                        //     layer.msg("昵称已被占用!");
                        //     flag2 = 0;
                        // }
                        if (flag2 == 1){
                            $.ajax({
                                url:'/judge/phone/to/update/password',
                                type:'post',
                                data:{
                                    username:$("#foreign_username").val(),
                                    phone:$("#foreign_phone").val()
                                },
                                datatype:'json',
                                success:function (data){
                                    if (data == "FAILED"){
                                        layer.msg("该手机号不属于该用户!");
                                        flag3 = 0;
                                    }
                                    if (flag3 == 1){
                                        $.ajax({
                                            url:'/get/short/message',
                                            type:'get',
                                            data:{
                                                phone:$("#foreign_phone").val(),
                                                message:$("#foreign_short_message_input").val()
                                            },
                                            datatype:'json',
                                            success:function (data){
                                                if (data == "codeNotMatch"){
                                                    layer.msg("验证码不正确!");
                                                    return;
                                                }else if (data == "codeTimeout"){
                                                    layer.msg("验证码已过期!")
                                                    return;
                                                }else if (data == "SUCCESS"){
                                                    $.ajax({
                                                        url:'/to/update/password',
                                                        type:'post',
                                                        data:{
                                                            username:$("#foreign_username").val(),
                                                            password:$("#foreign_password").val(),

                                                        },
                                                        success:function (data){
                                                            if (data == "SUCCESS"){
                                                                $("#foreignPasswordModal").init();
                                                                $("#foreignPasswordModal").hide();
                                                                $(".modal-backdrop").remove();
                                                                $("#loginModal").modal("show");
                                                                //注册后的提示
                                                                layer.msg("更新密码成功!");
                                                            }
                                                            if (data == "FAILED"){
                                                                layer.msg("更新失败,请稍后再试!");
                                                            }
                                                        }
                                                    })
                                                }else {
                                                    $("#message_register_short").text("请求超时!");
                                                }
                                            }
                                        })
                                    }
                                }
                            })
                        }
                    }
                })
            }
        }
    })
}




